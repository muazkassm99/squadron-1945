﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthSystem : HealthSystem
{
    public override void Die()
    {
        GameEvents.instance.PlayerDeath();
    }

    public override void HealthChanged()
    {
        GameEvents.instance.HealthChanged();
    }

}
