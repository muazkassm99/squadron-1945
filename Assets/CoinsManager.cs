﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class CoinsManager : MonoBehaviour
{

    private int coins = 0;
    private int oneCoinInScore = 200;
    
    private void Start()
    {
        GameEvents.instance.onLevelLose += calculateCoins;
        GameEvents.instance.onLevelWin += calculateCoins;
    }

    public void calculateCoins()
    {
        //get the score
        int score = GameObject.Find("GameManager").GetComponentInChildren<ScoreManager>().currentScore;
        //do math and calculate coins
        int calculatedCoins = score / oneCoinInScore;
        //add calculated coins to my coins
        Debug.Log("Coins " + calculatedCoins);
        this.coins += calculatedCoins;

    }

    public int GetCoins()
    {
        return this.coins;
    }
}
