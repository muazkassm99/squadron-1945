﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public Transform playerBody;

    public float mouseSensitivity = 100f;

    float xRotation = 0;
    float yRotation = 0;

    float zRotation = 0f;

    float rotSpeed = 10;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        /*
        //Rotate on the Y axis ( When mouse moves Horizontally )

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, +90f);

        //direction is only on the surface that the player is on
        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        */

        /* //Fly in all direction
         mouseY = Mathf.Clamp(mouseY, -90, 90);
         playerBody.Rotate(Vector3.right * -1 * mouseY);

         //Rotate on the X axis ( When mouse moves Vertically )

         playerBody.Rotate(Vector3.up * mouseX);

         //playerBody.Rotate(playerBody.transform.up * mouseX);
 */

        xRotation -= mouseY;
        //xRotation = Mathf.Clamp(xRotation, -90f, +90f);

        yRotation += mouseX;
        //yRotation = Mathf.Clamp(yRotation, -180, +180f);


/*
 *      
        zRotation += yRotation / 60 * 10;*/
        zRotation = Mathf.SmoothDamp(zRotation, -yRotation / 60 * 10, ref rotSpeed, 1);

        if(yRotation > 0)
        {
            zRotation = -30;
        }else if(yRotation < 0)
        {
            zRotation = +30;
        }


        //playerBody.transform.localRotation = Quaternion.Euler(xRotation, yRotation, zRotation);
        playerBody.transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0);
    }
}
