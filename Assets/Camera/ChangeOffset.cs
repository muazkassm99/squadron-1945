﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOffset : MonoBehaviour
{
    public Vector3 [] position;

    private int selectedIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        position = new Vector3[3];
        position[0] = new Vector3(0f, 7f, -17f);
        position[1] = new Vector3(0f, 3f, -22f);
        position[2] = new Vector3(0f, 3f, -6f);
        transform.localPosition = position[0];
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            selectedIndex++;
            if(selectedIndex >= position.Length)
            {
                selectedIndex = 0;
            }

            transform.localPosition = position[selectedIndex];
        }
    }
}
