﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class is responssible for spawning bots around stations
 * 
 * 
 */


public class BotsSpawner : MonoBehaviour
{

    public GameObject[] stations; //stations in scene
    public GameObject[] pathWays; //all athways in scene

    public GameObject botPrefab; //The bot prefab

    public int botsAroundStation = 5; //number of bots around a station


    //The box around the station which the bots are randomly spawned in.
    public Vector3 spawnBoundriesNear;
    public Vector3 spawnBoundriesFar;

    private void Awake()
    {
        spawnBoundriesFar = new Vector3(250, 0, 250);
        spawnBoundriesNear = new Vector3(200, 0, 200);
 
        GameEvents.instance.onStationSpawned += Instance_onStationSpawned;
    }

    private void Instance_onStationSpawned()
    { 
        //when stations are spawned get the stations and the pathways and then spawn bots.
        stations = GameObject.FindGameObjectsWithTag("Station");
        pathWays = GameObject.FindGameObjectsWithTag("PathWay");
        SpawnBots();
    }

    private void SpawnBots()
    {
        //for each station and path spawn bots.
        for (int i = 0; i < stations.Length; i++)
        {
            //Spawn bots arround this station
            SpawnBotsAroundMe(stations[i], pathWays[i]);
        }
    }

    private void SpawnBotsAroundMe(GameObject station, GameObject pathWay)
    {

        for (int i = 0; i < botsAroundStation; i++)
        {
            //Instatiate a bot near that object
            Vector3 spawnPosition = new Vector3(
                UnityEngine.Random.Range(-spawnBoundriesNear.x, spawnBoundriesFar.x),
                UnityEngine.Random.Range(-spawnBoundriesNear.y, spawnBoundriesFar.y),
                UnityEngine.Random.Range(-spawnBoundriesNear.z, spawnBoundriesFar.z)
                );
            //create a bot in near the station.
            GameObject bot = Instantiate(botPrefab, (station.transform.position + spawnPosition), Quaternion.identity);
            
            //Edit its PathWay
            bot.GetComponent<OpenWorldBotBehavior>().patrolPath = pathWay.GetComponent<WayPointsSystem>();
        }
        
        

    }
}
