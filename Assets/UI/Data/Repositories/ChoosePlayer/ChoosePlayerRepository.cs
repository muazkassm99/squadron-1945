using Space_Squadron.Assets.Data.Entities.ProgressData;
using System.Collections.Generic;

namespace Space_Squadron.Assets.Data.Repositories
{
    public interface ChoosePlayerRepository
    {
        void CreateAndLoadGuestPlayer();
        void CreateAndLoadNewPlayer(string playerName);

        IList<PlayerEntity> ShowCreatedPlayers();

        void LoadSelectedPlayer(string playerId);
    }
}