using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.Repositories.Settings
{
    public interface SettingsRepository
    {
        SettingsEntity GetCurrentPlayerSettings();
        void ApplySettings(SettingsEntity settings);
    }
}