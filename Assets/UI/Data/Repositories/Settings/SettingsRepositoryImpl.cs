using System;
using Space_Squadron.Assets.Data.DataSources.Player;
using Space_Squadron.Assets.Data.DataSources.Settings;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.FileManagers.Settings;

namespace Space_Squadron.Assets.Data.Repositories.Settings
{
    public class SettingsRepositoryImpl : SettingsRepository
    {

        private SettingsPersistentDataSource settingsPersistentDataSource;

        private SettingsCacheDataSource settingsCacheDataSource;

        private PlayerCacheDataSource playerCacheDataSource;
        private static SettingsRepositoryImpl instance;

        private SettingsRepositoryImpl() {
            settingsPersistentDataSource = SettingsPersistentDataSourceImpl.GetInstance();
            settingsCacheDataSource = SettingsCacheDataSourceImpl.GetInstance();
            playerCacheDataSource = PlayerCacheDataSourceImpl.GetInstance();
        }

        public static SettingsRepositoryImpl GetInstance() {
            if(instance == null) {
                instance = new SettingsRepositoryImpl();
            }
            return instance;
        }

        public SettingsEntity GetCurrentPlayerSettings()
        {
            string CurrentPlayerId = playerCacheDataSource.GetCachedPlayerId();
            return settingsPersistentDataSource.GetPlayerSettings(CurrentPlayerId);
        }

        public void ApplySettings(SettingsEntity settings)
        {
            settingsCacheDataSource.CachePrefs(settings);
            settingsCacheDataSource.ApplyInGameComponents(settings);
            settingsPersistentDataSource.SavePlayerSettings(settings);
        }
    }
}