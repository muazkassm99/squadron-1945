using Space_Squadron.Assets.Data.DataSources.Player;
using Space_Squadron.Assets.Data.DataSources.PlayerProgress;

namespace Space_Squadron.Assets.Data.Repositories.ChooseLevel
{
    public class ChooseLevelRepositoryImpl : ChooseLevelRepository
    {

        private PlayerCacheDataSource playerCacheDataSource;

        private PlayerProgressPersistentDataSource playerProgressPersistentDataSource;

        private static ChooseLevelRepositoryImpl instance;

        private ChooseLevelRepositoryImpl() {
            playerCacheDataSource = PlayerCacheDataSourceImpl.GetInstance();

        }

        public static ChooseLevelRepositoryImpl GetInstance() {
            if(instance == null) {
                instance = new ChooseLevelRepositoryImpl();
            }
            return instance;
        }
        public int LoadPlayerLevelProgressIndex()
        {
            string playerId = playerCacheDataSource.GetCachedPlayerId();
            return playerProgressPersistentDataSource.GetPlayerProgress(playerId).lastUnlockedLevelIndex;
        }
    }
}