namespace Space_Squadron.Assets.Data.Repositories.ChooseLevel
{
    public interface ChooseLevelRepository
    {
        int LoadPlayerLevelProgressIndex();
    }
}