using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.DataSources.SpaceShip;
using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.DataSources.Scene;
using Space_Squadron.Assets.Data.DataSources.Player;
using Space_Squadron.Assets.Data.DataSources.Settings;

namespace Space_Squadron.Assets.Data.Repositories
{
    public class InGameRepositoryImpl : InGameRepository
    {
        private static InGameRepositoryImpl instance;

        private SpaceShipCacheDataSource spaceShipCacheDataSource;
        private SpaceShipPersistentDataSource spaceShipPersistentDataSource;
        private ScenePersistentDataSource scenePersistentDataSource;
        private SceneCacheDataSource sceneCacheDataSource;
        private SettingsCacheDataSource settingsCacheDataSource;

        private PlayerCacheDataSource playerCacheDataSource;
        private InGameRepositoryImpl() {
            spaceShipCacheDataSource = SpaceShipCacheDataSourceImpl.GetInstance();
            spaceShipPersistentDataSource = SpaceShipPersistentDataSourceImpl.GetInstance();
            scenePersistentDataSource = ScenePersistentDataSourceImpl.GetInstance();
            sceneCacheDataSource = SceneCacheDataSourceImpl.GetInstance();
            settingsCacheDataSource = SettingsCacheDataSourceImpl.GetInstance();
            playerCacheDataSource = PlayerCacheDataSourceImpl.GetInstance();
        }

        public static InGameRepositoryImpl GetInstance() {
            if(instance == null) {
                return new InGameRepositoryImpl();
            } else {
                return instance;
            }
        }
        public SpaceShipEntity GetCurrentPlayerSpaceShip()
        {
            string currentPlayerId = playerCacheDataSource.GetCachedPlayerId();
            return spaceShipPersistentDataSource.GetCurrentSelectedSpaceShip(currentPlayerId);
        }

        public SceneEntity GetCurrentScene()
        {
            int sceneIndex = sceneCacheDataSource.GetCurrentSceneIndex();
            int sceneDifficulty = settingsCacheDataSource.GetCachedDifficulty();
            return scenePersistentDataSource.LoadScene(sceneIndex, sceneDifficulty);
        }
    }
}