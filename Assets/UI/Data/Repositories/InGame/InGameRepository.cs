using System.Runtime;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Entities.GameData;

namespace Space_Squadron.Assets.Data.Repositories
{
    public interface InGameRepository
    {
        SpaceShipEntity GetCurrentPlayerSpaceShip();

        SceneEntity GetCurrentScene();
    }
}