using System;

namespace Space_Squadron.Assets.Data.Entities.GameData
{
    [SerializableAttribute]
    public class SpaceStationEntity
    {
        public float score { get; set; }

        public float hp { get; set; }

        public float healthRegenerateRate { get; set; }

        public int level { get; set;    }

    }
}