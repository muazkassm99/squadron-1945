using System;
using System.Collections.Generic;

namespace Space_Squadron.Assets.Data.Entities.GameData
{
    [SerializableAttribute]
    public class SceneEntity
    {
        public int sceneIndex { get; set; }

        public int level { get; set; }

        public IList<SpaceShipEntity> enemyBots { get; set; }

        public IList<SpaceStationEntity> enemyStations { get; set; }

        public AstroidEntity astroid { get; set; }
    }
}