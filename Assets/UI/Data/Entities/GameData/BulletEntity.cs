using System;

namespace Space_Squadron.Assets.Data.Entities.GameData
{
    [SerializableAttribute]
    public class BulletEntity
    {
        public float damage { get; set; }
        public float speed { get; set; }
        public BulletType type { get; set; }
    }

    public enum BulletType : Byte {
        FORWARD = 0,
        FOLLOW = 1,
        LASER = 2
    }
}