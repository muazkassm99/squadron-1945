using System;

namespace Space_Squadron.Assets.Data.Entities.GameData
{
    [SerializableAttribute]
    public class WeaponEntity
    {
        public BulletEntity bullet { get; set; }
        public WeaponMultiplicity multiplicity { get; set; }
        public float fireRate { get; set; }
    }

    [Flags]
    public enum WeaponMultiplicity : byte
    {
        SINGLE = 0, DOUBLE = 1, TRIPLE = 2
    }
}