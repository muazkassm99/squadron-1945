using System;

namespace Space_Squadron.Assets.Data.Entities
{
    [Serializable]
    public class PlayerProgressEntity
    {
        public string playerId { get; set; }
        public int enemiesEliminated { get; set;}
        public int totalGameScore { get; set; }
        public int currentCoins { get; set; }
        public int lastUnlockedLevelIndex { get; set; }

    }
}