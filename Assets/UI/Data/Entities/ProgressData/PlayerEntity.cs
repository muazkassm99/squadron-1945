using System;
namespace Space_Squadron.Assets.Data.Entities.ProgressData
{
    [SerializableAttribute]
    public class PlayerEntity
    {
        public string id { get; set; }
        public string name { get; set; }
        public string playerProgressId { get; set; }
        public string playerSettingsId { get; set; }
        public bool isGuest { get; set; }
    }
}