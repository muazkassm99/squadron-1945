namespace Space_Squadron.Assets.Data.DataSources.Scene
{
    public interface SceneCacheDataSource
    {
        int GetCurrentSceneIndex();
    }
}