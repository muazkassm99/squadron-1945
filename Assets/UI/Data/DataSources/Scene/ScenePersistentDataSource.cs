using Space_Squadron.Assets.Data.Entities.GameData;

namespace Space_Squadron.Assets.Data.DataSources.Scene
{
    public interface ScenePersistentDataSource
    {
        SceneEntity LoadScene(int sceneIndex, int sceneDifficulty);
    }
}