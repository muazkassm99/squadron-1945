using Space_Squadron.Assets.Data.Constants;
using UnityEngine;

namespace Space_Squadron.Assets.Data.DataSources.Scene
{
    public class SceneCacheDataSourceImpl : SceneCacheDataSource
    {

        private static SceneCacheDataSourceImpl instance;

        private SceneCacheDataSourceImpl() {
            
        }

        public static SceneCacheDataSourceImpl GetInstance() {
            if(instance == null) {
                instance = new SceneCacheDataSourceImpl();
            }
            return instance;
        }

        public int GetCurrentSceneIndex()
        {
            return PlayerPrefs.GetInt(PlayerPrefsKeys.CURRENT_SELECTED_SCENE, 0);
        }
    }
}