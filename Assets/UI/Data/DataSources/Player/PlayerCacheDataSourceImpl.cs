using System;
using System.Numerics;
using Space_Squadron.Assets.Data.Constants;
using UnityEngine;

namespace Space_Squadron.Assets.Data.DataSources.Player
{
    public class PlayerCacheDataSourceImpl : PlayerCacheDataSource
    {

        private static PlayerCacheDataSourceImpl instance;

        private PlayerCacheDataSourceImpl() {
            
        }
        
        public static PlayerCacheDataSourceImpl GetInstance() {
            if(instance == null){
                instance = new PlayerCacheDataSourceImpl();
            }
            return instance;
        }

        public void CacheCurrentPlayerAsGuest(bool isGuest)
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.IS_GUEST, Convert.ToInt32(isGuest));
        }

        public void CacheCurrentPlayerId(string currentPlayerId)
        {
            PlayerPrefs.SetString(PlayerPrefsKeys.CURRENT_PLAYER_ID, currentPlayerId);
        }

        public void CacheCurrentPlayerName(string currentPlayerName)
        {
            PlayerPrefs.SetString(PlayerPrefsKeys.CURRENT_PLAYER_NAME, currentPlayerName);
        }

        public string GetCachedPlayerId()
        {
            return PlayerPrefs.GetString(PlayerPrefsKeys.CURRENT_PLAYER_ID, "NOT ASSIGNED");
        }

        public string GetCachedPlayerName()
        {
            return PlayerPrefs.GetString(PlayerPrefsKeys.CURRENT_PLAYER_NAME, "NOT ASSIGNED");
        }

        public bool IsCachedPlayerGuest()
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsKeys.IS_GUEST));
        }
    }
}