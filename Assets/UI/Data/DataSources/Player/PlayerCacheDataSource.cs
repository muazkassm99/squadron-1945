namespace Space_Squadron.Assets.Data.DataSources.Player
{
    public interface PlayerCacheDataSource
    {

        string GetCachedPlayerId();

        string GetCachedPlayerName();

        bool IsCachedPlayerGuest();
        
        void CacheCurrentPlayerId(string currentPlayerId);

        void CacheCurrentPlayerName(string currentPlayerName);

        void CacheCurrentPlayerAsGuest(bool isGuest);
    }
}