using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.DataSources.PlayerProgress
{
    public interface PlayerProgressPersistentDataSource
    {
        void EditCoins(string playerId, int cost);
        PlayerProgressEntity GetPlayerProgress(string playerId);
    }
}