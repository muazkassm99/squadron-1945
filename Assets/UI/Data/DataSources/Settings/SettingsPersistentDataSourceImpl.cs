using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.FileManagers.Settings;

namespace Space_Squadron.Assets.Data.DataSources.Settings
{
    public class SettingsPersistentDataSourceImpl : SettingsPersistentDataSource
    {
        private static SettingsPersistentDataSourceImpl instance;

        private SettingsFileManager fileManager;

        private SettingsPersistentDataSourceImpl() {
            fileManager = SettingsFileManagerImpl.GetInstance();
        }

        public static SettingsPersistentDataSourceImpl GetInstance() {
            if(instance == null) {
                instance = new SettingsPersistentDataSourceImpl();
            }
            return instance;
        }

        public SettingsEntity GetPlayerSettings(string currentPlayerId)
        {
            return fileManager.FindSettingsById(currentPlayerId);
        }

        public void SavePlayerSettings(SettingsEntity settings)
        {
            fileManager.SavePlayerSettings(settings);
        }
    }
}