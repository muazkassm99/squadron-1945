using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Constants;

using UnityEngine;

namespace Space_Squadron.Assets.Data.DataSources.SpaceShip
{
    public class SpaceShipCacheDataSourceImpl : SpaceShipCacheDataSource
    {

        private static SpaceShipCacheDataSourceImpl instance;

        private SpaceShipCacheDataSourceImpl() {
            
        }

        public static SpaceShipCacheDataSourceImpl GetInstance() {
            if(instance == null) {
                instance = new SpaceShipCacheDataSourceImpl();
            }
            return instance;
        }

        public void CacheSelectedSpaceShip(string spaceShipId)
        {
            PlayerPrefs.SetString(PlayerPrefsKeys.CURRENT_PLAYER_SPACE_SHIP, spaceShipId);
        }

        public string GetCurrentPlayerSpaceShipId()
        {
            return PlayerPrefs.GetString(PlayerPrefsKeys.CURRENT_PLAYER_SPACE_SHIP, "NOT ASSIGNED");
        }
    }
}