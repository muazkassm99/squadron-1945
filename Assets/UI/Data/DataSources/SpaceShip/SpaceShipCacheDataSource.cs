using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.DataSources.SpaceShip
{
    public interface SpaceShipCacheDataSource
    {
        string GetCurrentPlayerSpaceShipId();
        void CacheSelectedSpaceShip(string spaceShipId);
    }
}