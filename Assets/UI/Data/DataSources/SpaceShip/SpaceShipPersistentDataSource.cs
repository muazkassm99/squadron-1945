using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.DataSources.SpaceShip
{
    public interface SpaceShipPersistentDataSource
    {
        IList<SpaceShipEntity> GetAllPlayerSpaceShips(string playerId);
        void PurchaseNewSpaceShip(string playerId, string spaceShipId);
        SpaceShipEntity GetCurrentSelectedSpaceShip(string currentPlayerId);
        void selectCamoOnSpaceShip(string spaceShipId, int camoIndex);
        void SelectSpaceShip(string currentPlayerId, string spaceShipId);
    }
}