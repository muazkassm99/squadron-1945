using UnityEngine;
using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.Entities;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Space_Squadron.Assets.Data.FileManagers.Settings
{
    public class SettingsFileManagerImpl : SettingsFileManager
    {

        private DefaultPlayerEntityGenerator generator;
        private static SettingsFileManagerImpl instance;

        private SettingsFileManagerImpl() {
            generator = new DefaultPlayerEntityGenerator();
        }

        public static SettingsFileManagerImpl GetInstance() {
            if(instance == null) {
                instance = new SettingsFileManagerImpl();
            }
            return instance;
        }

        public SettingsEntity FindSettingsById(string playerId)
        {
            Directory.CreateDirectory(Application.persistentDataPath + FileEndPoints.PLAYER_SETTINGS_PATH);
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_SETTINGS_PATH + playerId + ".save";
            if(!File.Exists(path)) {
                CreateDefaultPlayerSettingsFile(playerId);
            }
            return ReadFile(playerId);
        }

        private void CreateDefaultPlayerSettingsFile(string playerId) {
            
            SettingsEntity created = generator.Generate(playerId);
            WriteFile(created);
        }

        private class DefaultPlayerEntityGenerator
        {
            public SettingsEntity Generate(string playerId)
            {
                return new SettingsEntity() { 
                    id = playerId,
                    displayResolutionWidth = 1280,
                    displayResolutionHeight = 720,
                    graphicsQuality = 1,
                    controlType = 0,
                    soundVolume = -40,
                    gameDifficulty = 0
                };
            }
        }

        private SettingsEntity ReadFile(string settingsId)
        {
            SettingsEntity extracted = null;
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_SETTINGS_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using(FileStream fileStream = new FileStream(path + settingsId + ".save", FileMode.Open)) {
                extracted = (SettingsEntity) formatter.Deserialize(fileStream);
                fileStream.Close();
            }
            return extracted;
        }

        private void WriteFile(SettingsEntity settings)
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_SETTINGS_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using(FileStream fileStream = new FileStream(path + settings.id + ".save", FileMode.Create)) {
                formatter.Serialize(fileStream, settings);
                fileStream.Close();
            }
        }

        public void SavePlayerSettings(SettingsEntity settings)
        {
            WriteFile(settings);
        }
    }
}