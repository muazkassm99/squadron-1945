using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.FileManagers.PlayerSpaceShip;
using UnityEngine;

namespace Space_Squadron.Assets.Data.FileManagers.Scene
{
    public class SceneFileManagerImpl : SceneFileManager
    {

        private static SceneFileManagerImpl instance;

        private SceneGenerator sceneGenerator;

        private SceneFileManagerImpl() {
            sceneGenerator = new SceneGenerator();
        }

        public static SceneFileManagerImpl GetInstance() {
            if(instance == null) {
                instance = new SceneFileManagerImpl();
            }
            return instance;
        }

        public SceneEntity ReadFile(int sceneIndex, int level)
        {
            string path = Application.persistentDataPath + FileEndPoints.SCENE_PATH + sceneIndex + level + ".scene";
            
            if(!File.Exists(path)) {
                switch(sceneIndex) {
                    case 0: WriteFile(sceneGenerator.GenerateScene0(level)); break;
                }
            }

            SceneEntity extracted = null;
            BinaryFormatter formatter = new BinaryFormatter();
            using(FileStream fileStream = new FileStream(path, FileMode.Open)) {
                extracted = (SceneEntity) formatter.Deserialize(fileStream);
            }
            return extracted;
        }

        public void WriteFile(SceneEntity extracted)
        {
            string path = Application.persistentDataPath + FileEndPoints.SCENE_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using(FileStream fileStream = new FileStream(path + extracted.sceneIndex + extracted.level + ".scene", FileMode.Create)) {
                formatter.Serialize(fileStream, extracted);
            }
        }

        private class SceneGenerator {

            BotSpaceShipGenerator botSpaceShipGenerator;
            SpaceStationGenerator spaceStationGenerator;
            AstroidGenerator astroidGenerator;
            

            public SceneGenerator() {
                botSpaceShipGenerator = new BotSpaceShipGenerator();
                spaceStationGenerator = new SpaceStationGenerator();
                astroidGenerator = new AstroidGenerator();
            }

            public SceneEntity GenerateScene0(int level) {
                IList<SpaceShipEntity> bots = new List<SpaceShipEntity>();
                bots.Add(botSpaceShipGenerator.GenerateBot1SpaceShip(level));
                bots.Add(botSpaceShipGenerator.GenerateBot2SpaceShip(level));
                IList<SpaceStationEntity> spaceStations = new List<SpaceStationEntity>();
                spaceStations.Add(spaceStationGenerator.GenerateSpaceStation1(level));
                AstroidEntity astroid = astroidGenerator.GenerateAstroid(level);
                return new SceneEntity() {sceneIndex = 0, level = level, enemyBots = bots, enemyStations = spaceStations, astroid = astroid};
            }
            
            private class BotSpaceShipGenerator {
                public SpaceShipEntity GenerateBot1SpaceShip(int level) {
                    return level == 0 ? easy1() : level == 1 ? medium1() : level == 2 ? hard1() : null;
                }
                private SpaceShipEntity easy1() => new SpaceShipEntity() { level = 0, scoreValue = 1200, hp = 100, healthRegen = 3, shield = 10, minSpeed = 20, maxSpeed = 50, sprintSpeed = 150,
                    weapon = new WeaponEntity() { multiplicity = WeaponMultiplicity.SINGLE, fireRate = 0.5f, bullet = new BulletEntity() { damage = 15, speed = 200, type = BulletType.FORWARD}}};
                private SpaceShipEntity medium1() => new SpaceShipEntity() { level = 1, scoreValue = 1400, hp = 150, healthRegen = 5, shield = 20, minSpeed = 20, maxSpeed = 50, sprintSpeed = 150,
                    weapon = new WeaponEntity() { multiplicity = WeaponMultiplicity.SINGLE, fireRate = 0.4f, bullet = new BulletEntity() { damage = 20, speed = 200, type = BulletType.FORWARD}}};
                private SpaceShipEntity hard1() => new SpaceShipEntity() { level = 2, scoreValue = 1600, hp = 150, healthRegen = 7, shield = 30, minSpeed = 20, maxSpeed = 50, sprintSpeed = 150,
                    weapon = new WeaponEntity() { multiplicity = WeaponMultiplicity.SINGLE, fireRate = 0.45f, bullet = new BulletEntity() { damage = 25, speed = 200, type = BulletType.FORWARD}}};
                public SpaceShipEntity GenerateBot2SpaceShip(int level) {
                    return level == 0 ? easy2() : level == 1 ? medium2() : level == 2 ? hard2() : null;
                }
                private SpaceShipEntity easy2() => new SpaceShipEntity() { level = 0, scoreValue = 1200, hp = 100, healthRegen = 3, shield = 10, minSpeed = 20, maxSpeed = 50, sprintSpeed = 150,
                    weapon = new WeaponEntity() { multiplicity = WeaponMultiplicity.SINGLE, fireRate = 0.5f, bullet = new BulletEntity() { damage = 15, speed = 200, type = BulletType.FORWARD}}};
                private SpaceShipEntity medium2() => new SpaceShipEntity() { level = 1, scoreValue = 1400, hp = 150, healthRegen = 5, shield = 20, minSpeed = 20, maxSpeed = 50, sprintSpeed = 150,
                    weapon = new WeaponEntity() { multiplicity = WeaponMultiplicity.SINGLE, fireRate = 0.4f, bullet = new BulletEntity() { damage = 20, speed = 200, type = BulletType.FORWARD}}};
                private SpaceShipEntity hard2() => new SpaceShipEntity() { level = 2, scoreValue = 1600, hp = 150, healthRegen = 7, shield = 30, minSpeed = 20, maxSpeed = 50, sprintSpeed = 150,
                    weapon = new WeaponEntity() { multiplicity = WeaponMultiplicity.SINGLE, fireRate = 0.45f, bullet = new BulletEntity() { damage = 25, speed = 200, type = BulletType.FORWARD}}};
            }

            private class SpaceStationGenerator {
                public SpaceStationEntity GenerateSpaceStation1(int level) {
                    return level == 0 ? easy1() : level == 1 ? medium1() : level == 2 ? hard1() : null;
                }

                private SpaceStationEntity easy1() => new SpaceStationEntity() { level = 0, score = 1200, hp = 500, healthRegenerateRate = 10};
                private SpaceStationEntity medium1() => new SpaceStationEntity() { level = 1, score = 1400, hp = 700, healthRegenerateRate = 15};
                private SpaceStationEntity hard1() => new SpaceStationEntity() { level = 2, score = 1600, hp = 1000, healthRegenerateRate = 20};
            }

            private class AstroidGenerator {
                public AstroidEntity GenerateAstroid(int level) {
                    return level == 0 ? easy() : level == 1 ? medium() : level == 2 ? hard() : null;
                }

                private AstroidEntity easy() => new AstroidEntity() { level = 0, scoreValue = 33, hp = 120};
                private AstroidEntity medium() => new AstroidEntity() { level = 1, scoreValue = 44, hp = 150};
                private AstroidEntity hard() => new AstroidEntity() { level = 2, scoreValue = 55, hp = 180};
            }
        }
        



    }
}