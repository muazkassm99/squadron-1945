using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Space_Squadron.Assets.Data.Entities.ProgressData;
using Space_Squadron.Assets.Data.Util;
using Space_Squadron.Assets.Data.Constants;

namespace Space_Squadron.Assets.Data.FileManagers
{
    public class PlayerFileManagerImpl : PlayerFileManager
    {

        private static PlayerFileManagerImpl instance;
        
        private PlayerFileManagerImpl() {
            generator = new DefaultPlayerEntityGenerator();
        }

        public static PlayerFileManagerImpl GetInstance() {
            if(instance == null) {
                instance = new PlayerFileManagerImpl();
            }
            return instance;
        }
        private DefaultPlayerEntityGenerator generator;

        private void WriteFile(PlayerEntity player)
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using(FileStream fileStream = new FileStream(path + player.id + ".save", FileMode.Create)) {
                formatter.Serialize(fileStream, player);
                fileStream.Close();
            }
        }

        private PlayerEntity ReadFile(string playerId)
        {
            PlayerEntity extracted = null;
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using(FileStream fileStream = new FileStream(path + playerId + ".save", FileMode.Open)) {
                extracted = (PlayerEntity) formatter.Deserialize(fileStream);
                fileStream.Close();
            }
            return extracted;
        }

        private class DefaultPlayerEntityGenerator
        {
            public PlayerEntity Generate(string name, bool isGuest)
            {
                string id = TimeBasedIdGenerator.GenerateId();
                return new PlayerEntity() { 
                    id = id,
                    name = name,
                    playerProgressId = id,
                    playerSettingsId = id,
                    isGuest = isGuest
                };
            }
        }
        public PlayerEntity CreateNewPlayerFile(string playerName, bool isGuest)
        {
            PlayerEntity createdPlayer = generator.Generate(playerName, isGuest);
            WriteFile(createdPlayer);
            return createdPlayer;
        }
        
        public IList<PlayerEntity> GetAllPlayers()
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_PATH;
            Directory.CreateDirectory(path);
            string[] saveFiles = Directory.GetFiles(Application.persistentDataPath + FileEndPoints.PLAYER_PATH);
            IList<PlayerEntity> players = new List<PlayerEntity>();
            BinaryFormatter formatter = new BinaryFormatter();
            foreach (string fileDirectory in saveFiles) {
                using(FileStream fileStream = new FileStream(fileDirectory, FileMode.Open)) {
                    PlayerEntity player = (PlayerEntity)formatter.Deserialize(fileStream);
                    if(!player.isGuest) players.Add(player);
                }
            }
            return players;
        }

        public PlayerEntity FindPlayerById(string playerId)
        {
            return ReadFile(playerId);
        }

        
    }
}