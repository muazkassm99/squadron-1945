using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities.ProgressData;

namespace Space_Squadron.Assets.Data.FileManagers
{
    public interface PlayerFileManager
    {
        PlayerEntity CreateNewPlayerFile(string playerName, bool isGuest);
        IList<PlayerEntity> GetAllPlayers();
        PlayerEntity FindPlayerById(string playerId);
    }
}