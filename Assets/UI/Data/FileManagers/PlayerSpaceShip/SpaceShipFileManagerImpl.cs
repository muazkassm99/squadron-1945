using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.UI.Data.Constants;
using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.DataSources.SpaceShip;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.Util;
using TMPro;
using UnityEngine;
using System;

namespace Space_Squadron.Assets.Data.FileManagers.PlayerSpaceShip
{
    public class SpaceShipFileManagerImpl : SpaceShipFileManager
    {
        private static SpaceShipFileManagerImpl instance;
        private PlayerSpaceShipGenerator spaceShipGenerator;

        private SpaceShipFileManagerImpl() {
            spaceShipGenerator = new PlayerSpaceShipGenerator();
        }

        public static SpaceShipFileManagerImpl GetInstance() {
            if(instance == null) {
                instance = new SpaceShipFileManagerImpl();
            }
            return instance;
        }

        public IList<SpaceShipEntity> GetPlayerSpaceShips(string playerId)
        {
            Directory.CreateDirectory(Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH);
            string[] saveFiles = Directory.GetFiles(Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH);
            BinaryFormatter formatter = new BinaryFormatter();
            List<SpaceShipEntity> spaceShips = new List<SpaceShipEntity>();
            foreach (string fileDirectory in saveFiles) {
                using(FileStream fileStream = new FileStream(fileDirectory, FileMode.Open)) {
                    SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(fileStream);
                    if(spaceShip.playerId == playerId) spaceShips.Add(spaceShip);
                }
            }
            spaceShips.Sort((x, y) => x.index - y.index);
            if(spaceShips.Count < 4) {
                IList<bool> spaceShipFileExist = new List<bool>();
                for (int i = 0; i < GameConstants.MAX_PLAYER_SPACESHIPS; i++) spaceShipFileExist.Add(false);
                foreach(SpaceShipEntity spaceShip in spaceShips) {
                    spaceShipFileExist[spaceShip.index] = true;
                }
                for(int i = 0; i < GameConstants.MAX_PLAYER_SPACESHIPS; i++) {
                    if(!spaceShipFileExist[i]) { WriteFile(spaceShipGenerator.GenerateDefaultSpaceShip(playerId, i)); continue; }
                }
                return GetPlayerSpaceShips(playerId);
            } else {
                bool isOneSelected = false;
                bool isOneUnlocked = false;
                foreach(SpaceShipEntity spaceShip in spaceShips) {
                    if(spaceShip.isSelected) isOneSelected = true;
                    if (spaceShip.isUnlocked) isOneUnlocked = true;
                }
                SpaceShipEntity firstSpaceShip = null;
                firstSpaceShip = spaceShips[0];
                if(!isOneSelected) {
                    firstSpaceShip.isSelected = true;
                }
                if(!isOneUnlocked) {
                    firstSpaceShip.isUnlocked = true;
                }
                WriteFile(firstSpaceShip);
                return spaceShips;
            }
        }

        public SpaceShipEntity ReadFile(string spaceShipId)
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH + spaceShipId + ".save";
            SpaceShipEntity extracted = null;
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                extracted = (SpaceShipEntity)formatter.Deserialize(fileStream);
            }
            return extracted;
        }

        public void WriteFile(SpaceShipEntity extracted)
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(path + extracted.id + ".save", FileMode.Create))
            {
                formatter.Serialize(fileStream, extracted);
            }
        }

        //public void modifySelectedSpaceShipCamo(string spaceShipId, int camoIndex)
        //{
        //    string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH;
        //    Directory.CreateDirectory(path);
        //    string[] saveFiles = Directory.GetFiles(path);
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    SpaceShipEntity extracted = null;

        //    foreach (string fileDirectory in saveFiles)
        //    {
        //        if (fileDirectory.Equals(path + spaceShipId + ".save"))
        //        {
        //            using (FileStream read = new FileStream(fileDirectory, FileMode.Open))
        //            {
        //                extracted = (SpaceShipEntity)formatter.Deserialize(read);
        //            }
        //        }
        //    }

        //    extracted.isUnlocked = true;

        //    using (FileStream write = new FileStream(path + extracted.id + ".save", FileMode.Create))
        //    {
        //        formatter.Serialize(write, extracted);
        //    }

        //    foreach (string fileDirectory in saveFiles)
        //    {
        //        using (FileStream read = new FileStream(fileDirectory, FileMode.Open))
        //        {
        //            SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(read);
        //            if (spaceShip.id == spaceShipId)
        //            {
        //                extracted = spaceShip;
        //                break;
        //            }
        //        }
        //    }

        //    extracted.isUnlocked = true;

        //    using (FileStream write = new FileStream(path + extracted.id + ".save", FileMode.Create))
        //    {
        //        formatter.Serialize(write, extracted);
        //    }
        //}

        // public SpaceShipEntity GetSpaceShip(string spaceShipId)
        // {
        //     string[] spaceShipSaveFiles = Directory.GetFiles(Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH);
        //     BinaryFormatter formatter = new BinaryFormatter();
        //     foreach (string fileDirectory in spaceShipSaveFiles) {
        //         using(FileStream fileStream = new FileStream(fileDirectory, FileMode.Open)) {
        //             SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(fileStream);
        //             if(spaceShip.id == spaceShipId) return spaceShip;
        //         }
        //     }
        //     return null;
        // }

        //public void UnlockSpaceShip(string spaceShipId)
        //{
        //    string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH;
        //    Directory.CreateDirectory(path);
        //    string[] saveFiles = Directory.GetFiles(path);
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    SpaceShipEntity extracted = null;

        //    foreach (string fileDirectory in saveFiles) {
        //        using(FileStream read = new FileStream(fileDirectory, FileMode.Open)) {
        //            SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(read);
        //            if(spaceShip.id == spaceShipId) 
        //            {
        //                extracted = spaceShip;
        //                break;
        //            } 
        //        }
        //    }

        //    extracted.isUnlocked = true;

        //    using(FileStream write = new FileStream(path + extracted.id + ".save", FileMode.Create)) {
        //        formatter.Serialize(write, extracted);
        //    }
        //}

        private class PlayerSpaceShipGenerator {
            public SpaceShipEntity GenerateDefaultSpaceShip(string playerId, int index) {
                return new SpaceShipEntity() {
                    playerId = playerId,
                    id = TimeBasedIdGenerator.GenerateId(),
                    level = 1,
                    hp = 300,
                    healthRegen = 5,
                    shield = 30,
                    minSpeed = 20,
                    maxSpeed = 50,
                    sprintSpeed = 150,
                    weapon = new WeaponEntity() {
                        multiplicity = WeaponMultiplicity.SINGLE,
                        fireRate = 0.45f,
                        bullet = new BulletEntity() {
                            damage = 25,
                            speed = 200,
                            type = BulletType.FORWARD
                        }
                    },
                    isUnlocked = false,
                    index = index,
                    levelPrices = new List<int>() { 1000, 3500, 6500 }
                };
        
            }
        }
    }
}