using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.FileManagers.PlayerSpaceShip
{
    public interface SpaceShipFileManager
    {
        IList<SpaceShipEntity> GetPlayerSpaceShips(string playerId);
        SpaceShipEntity ReadFile(string spaceShipId);
        void WriteFile(SpaceShipEntity extracted);
    }
}