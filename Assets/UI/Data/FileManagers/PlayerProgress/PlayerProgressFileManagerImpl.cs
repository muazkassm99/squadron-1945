using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.Entities;
using UnityEngine;

namespace Space_Squadron.Assets.Data.FileManagers.PlayerProgress
{
    public class PlayerProgressFileManagerImpl : PlayerProgressFileManager
    {

        private static PlayerProgressFileManagerImpl instance;

        private DefaultPlayerProgressEntityGenerator generator;

        private PlayerProgressFileManagerImpl() {
            generator = new DefaultPlayerProgressEntityGenerator();
        }

        public static PlayerProgressFileManagerImpl GetInstance() {
            if(instance == null) {
                instance = new PlayerProgressFileManagerImpl();
            }
            return instance;
        }

        public void EditCoins(string playerId, int coins)
        {
            PlayerProgressEntity extracted = ReadFile(playerId);
            extracted.currentCoins += coins;
            WriteFile(extracted);
        }

        public int GetPlayerLevelIndex(string playerId)
        {
            PlayerProgressEntity extracted = ReadFile(playerId);
            return extracted.lastUnlockedLevelIndex;
        }

        public PlayerProgressEntity ReadFile(string playerId)
        {
            PlayerProgressEntity extracted = null;
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_PROGRESS_PATH;
            Directory.CreateDirectory(path);
            if (!File.Exists(path))
            {
                CreateDefaultPlayerProgressFile(playerId);
            }
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(path + playerId + ".save", FileMode.Open))
            {
                extracted = (PlayerProgressEntity)formatter.Deserialize(fileStream);
            }
            return extracted;
        }

        private void CreateDefaultPlayerProgressFile(string playerId)
        {
            PlayerProgressEntity generated = generator.Generate(playerId);
            WriteFile(generated);
        }

        public void WriteFile(PlayerProgressEntity extracted)
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_PROGRESS_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(path + extracted.playerId + ".save", FileMode.Create))
            {
                formatter.Serialize(fileStream, extracted);
            }
        }

        private class DefaultPlayerProgressEntityGenerator
        {
            public PlayerProgressEntity Generate(string playerId)
            {
                return new PlayerProgressEntity()
                {
                    playerId = playerId,
                };
            }
        }




    }
}