using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.FileManagers.PlayerProgress
{
    public interface PlayerProgressFileManager
    {

        void EditCoins(string playerId, int coins);
        int GetPlayerLevelIndex(string playerId);
        PlayerProgressEntity ReadFile(string playerId);
        void WriteFile(PlayerProgressEntity extracted);
    }
}