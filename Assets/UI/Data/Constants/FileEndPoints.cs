namespace Space_Squadron.Assets.Data.Constants
{
    public static class FileEndPoints
    {
        public const string PLAYER_PATH = "/Progress/Players/";

        public const string PLAYER_PROGRESS_PATH = "/Progress/PlayersProgress/";

        public const string PLAYER_SETTINGS_PATH = "/Progress/PlayerSettings/";
        
        public const string PLAYER_SPACESHIP_PATH = "/Progress/SpaceShips/";

        public const string SCENE_PATH = "/GameData/Scenes/";
    }
}