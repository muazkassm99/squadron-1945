namespace Space_Squadron.Assets.Data.Constants
{
    public static class PlayerPrefsKeys
    {
        public const string CURRENT_PLAYER_SPACE_SHIP = "CURRENT_PLAYER_SPACE_SHIP";

        public const string CURRENT_PLAYER_ID = "CURRENT_PLAYER_ID";

        public const string CURRENT_PLAYER_NAME = "CURRENT_PLAYER_NAME";

        public const string IS_GUEST = "IS_GUEST";

        // public const string CURRENT_RESOLUTION_CHOICE = "CURRENT_RESOLUTION_CHOICE";

        // public const string CURRENT_GRAHPICS_QUALITY_CHOICE = "CURRENT_GRAHPICS_QUALITY_CHOICE";

        public const string CURRENT_CONTROL_TYPE = "CURRENT_CONTROL_TYPE";

        // public const string CURRENT_SOUND_VOLUME_CHOICE = "CURRENT_SOUND_VOLUME_CHOICE";

        public const string CURRENT_DIFFICULTY_CHOICE = "CURRENT_DIFFICULTY_CHOICE";

        public const string CURRENT_SELECTED_SCENE = "CURRENT_SELECTED_SCENE";
    }
}