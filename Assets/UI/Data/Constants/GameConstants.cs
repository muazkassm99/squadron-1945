﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.UI.Data.Constants
{
    public static class GameConstants
    {
        public const int MAX_IN_GAME_PLAYERS = 4;

        public const int MAX_PLAYER_SPACESHIPS = 4;
    }
}
