﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Space_Squadron.Assets.Data.Repositories;
using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.Entities.ProgressData;
using UnityEngine.UI;
using TMPro;

public class ChoosePlayerMenuManager : MonoBehaviour
{

    private ChoosePlayerRepository choosePlayerRepository;
    private IList<PlayerEntity> players;
    public TMP_Text Save1Text;
    public TMP_Text Save2Text;
    public TMP_Text Save3Text;

    public GameObject DeleteButton1;

    public GameObject DeleteButton2;

    public GameObject DeleteButton3;

    void Awake() {
        choosePlayerRepository = ChoosePlayerRepositoryImpl.GetInstance();
    }

    void OnEnable() {
        Debug.Log("choose enabled");

        players = choosePlayerRepository.ShowCreatedPlayers();
        if(players.Count > 0)
            Save1Text.text = players[0].name;
            DeleteButton1.SetActive(true);
        if(players.Count > 1)
            Save2Text.text = players[1].name;
            DeleteButton2.SetActive(true);
        if(players.Count > 2)
            Save3Text.text = players[2].name;
            DeleteButton3.SetActive(true);
    }


    public void createGuestAccount() {
        choosePlayerRepository.CreateAndLoadGuestPlayer();
        MenuEvents.instance.PlayerChosen();
    }

    public void CreateNewPlayer(string playerName) {
        choosePlayerRepository.CreateAndLoadNewPlayer(playerName);
        MenuEvents.instance.PlayerChosen();
    }

    public void Save1OnClick() {
        if(players.Count == 0) {
            MenuEvents.instance.NewPlayer();
        } else {
            choosePlayerRepository.LoadSelectedPlayer(players[0].id);
            MenuEvents.instance.PlayerChosen();
        }
    }

    public void Save2OnClick() {
        if(players.Count == 1) {
            MenuEvents.instance.NewPlayer();
        } else {
            choosePlayerRepository.LoadSelectedPlayer(players[1].id);
            MenuEvents.instance.PlayerChosen();
        }
    }

    public void Save3OnClick() {
        if(players.Count == 2) {
            MenuEvents.instance.NewPlayer();
        } else {
            choosePlayerRepository.LoadSelectedPlayer(players[2].id);
            MenuEvents.instance.PlayerChosen();
        }
    }

}
