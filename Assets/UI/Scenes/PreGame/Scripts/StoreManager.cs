﻿using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Net.Security;
using TMPro;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StoreManager : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    private TMP_Text coinsText;

    [SerializeField]
    private GameObject NoEnoughMoneyDialogPanel;

    [SerializeField]
    private Button selectionButton1;
    [SerializeField]
    private Button selectionButton2;
    [SerializeField]
    private Button selectionButton3;
    [SerializeField]
    private Button selectionButton4;

    [SerializeField]
    private GameObject BuySpaceShipPanel;

    [SerializeField]
    private TMP_Text selectionText1;
    [SerializeField]
    private TMP_Text selectionText2;
    [SerializeField]
    private TMP_Text selectionText3;
    [SerializeField]
    private TMP_Text selectionText4;

    [SerializeField]
    private GameObject SpaceShip1;
    [SerializeField]
    private GameObject SpaceShip2;
    [SerializeField]
    private GameObject SpaceShip3;
    [SerializeField]
    private GameObject SpaceShip4;

    [SerializeField]
    private GameObject CamoGameObject1;
    [SerializeField]
    private GameObject CamoGameObject2;
    [SerializeField]
    private GameObject CamoGameObject3;
    [SerializeField]
    private GameObject CamoGameObject4;
    [SerializeField]
    private GameObject CamoGameObject5;
    [SerializeField]
    private GameObject CamoGameObject6;
    [SerializeField]
    private GameObject CamoGameObject7;

    [SerializeField]
    private Material CamoMaterial1;
    [SerializeField]
    private Material CamoMaterial2;
    [SerializeField]
    private Material CamoMaterial3;
    [SerializeField]
    private Material CamoMaterial4;
    [SerializeField]
    private Material CamoMaterial5;
    [SerializeField]
    private Material CamoMaterial6;
    [SerializeField]
    private Material CamoMaterial7;

    [SerializeField]
    private GameObject UpgradeButtonContainer1;
    [SerializeField]
    private GameObject UpgradeButtonContainer2;
    [SerializeField]
    private GameObject UpgradeButtonContainer3;
    [SerializeField]
    private GameObject UpgradeButtonContainer4;
    [SerializeField]
    private Button UpgradeButton1;
    [SerializeField]
    private Button UpgradeButton2;
    [SerializeField]
    private Button UpgradeButton3;
    [SerializeField]
    private Button UpgradeButton4;
    [SerializeField]
    private TMP_Text UpgradeButtonText1;
    [SerializeField]
    private TMP_Text UpgradeButtonText2;
    [SerializeField]
    private TMP_Text UpgradeButtonText3;
    [SerializeField]
    private TMP_Text UpgradeButtonText4;


    // Buisness variable
    private StoreRepository storeRepository;

    private List<SpaceShipEntity> playerSpaceShips;

    private int currentSelectedSpaceShip;

    //events
    private Action<int> OnSpaceShipSelected;

    private Action<int> OnCamoSelected;
    

    private void Awake()
    {
        storeRepository = StoreRepositoryImpl.GetInstance();

        OnSpaceShipSelected += ChangeSelectionButtonsText;
        OnSpaceShipSelected += SaveSelection;

        OnCamoSelected += ApplyCamoOnSelectedSpaceShip;
        OnCamoSelected += SaveCamoOnSelectedSpaceShip;
    }

    private void OnEnable()
    {
        UpdatePlayerSpaceShips();
        UpdateSpaceShipRelatedUI();
        OnSpaceShipSelected?.Invoke(currentSelectedSpaceShip);
        ApplyAllSpaceShipCamo();
        UpdateCoins();
    }

    private void UpdatePlayerSpaceShips()
    {
        playerSpaceShips = (List<SpaceShipEntity>) storeRepository.LoadPlayerSpaceShips();
        playerSpaceShips.Sort((x, y) => x.index - y.index);
        foreach (SpaceShipEntity spaceship in playerSpaceShips) if (spaceship.isSelected) currentSelectedSpaceShip = spaceship.index;
    }

    private void UpdateSpaceShipRelatedUI()
    {
        foreach (SpaceShipEntity spaceship in playerSpaceShips)
        {
            if (spaceship.index == 0)
            {
                if (spaceship.isUnlocked)
                {
                    selectionText1.text = "Select";
                    UpgradeButtonContainer1.SetActive(true);
                    UpgradeButtonText1.text = $"level {spaceship.level} - Upgrade ${spaceship.levelPrices[spaceship.level]}";
                }
                else
                {
                    UpgradeButtonContainer1.SetActive(false);
                    selectionText1.text = $"Buy - ${spaceship.price}";
                }
            }
            else if (spaceship.index == 1)
            {
                if (spaceship.isUnlocked)
                {
                    selectionText2.text = "Select";
                    UpgradeButtonContainer2.SetActive(true);
                    UpgradeButtonText2.text = $"level {spaceship.level} - Upgrade ${spaceship.levelPrices[spaceship.level]}";
                }
                else
                {
                    UpgradeButtonContainer2.SetActive(false);
                    selectionText2.text = $"Buy - ${spaceship.price}";
                }
            }
            else if (spaceship.index == 2)
            {
                if (spaceship.isUnlocked)
                {
                    selectionText3.text = "Select";
                    UpgradeButtonContainer3.SetActive(true);
                    UpgradeButtonText3.text = $"level {spaceship.level} - Upgrade ${spaceship.levelPrices[spaceship.level]}";
                }
                else
                {
                    UpgradeButtonContainer3.SetActive(false);
                    selectionText3.text = $"Buy - ${spaceship.price}";
                }
            }
            else if (spaceship.index == 3)
            {
                if (spaceship.isUnlocked)
                {
                    selectionText4.text = "Select";
                    UpgradeButtonContainer4.SetActive(true);
                    UpgradeButtonText4.text = $"level {spaceship.level} - Upgrade ${spaceship.levelPrices[spaceship.level]}";
                }
                else
                {
                    UpgradeButtonContainer4.SetActive(false);
                    selectionText4.text = $"Buy - ${spaceship.price}";
                }
            }
        }

    }

    private void ChangeSelectionButtonsText(int index)
    {
        switch (index)
        {
            case 0:
                selectionText1.text = "Selected";
                break;
            case 1:
                selectionText2.text = "Selected";
                break;
            case 2:
                selectionText3.text = "Selected";
                break;
            case 3:
                selectionText4.text = "Selected";
                break;
        }
    }

    private void SaveSelection(int index)
    {
        string spaceShipId = playerSpaceShips[index].id;
        storeRepository.SelectSpaceShip(spaceShipId);
        currentSelectedSpaceShip = index;
    }

    private void ApplyAllSpaceShipCamo()
    {
        foreach (SpaceShipEntity spaceShip in playerSpaceShips)
        {
            if (!spaceShip.isUnlocked) continue;
            int camoIndex = spaceShip.camoIndex;
            int spaceShipIndex = spaceShip.index;
            ApplyCamo(spaceShipIndex, camoIndex);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {

        // camo selecting logic
        int camoGameObjectId = eventData.pointerCurrentRaycast.gameObject.GetInstanceID();

        if (camoGameObjectId == CamoGameObject1.GetInstanceID()) {
            OnCamoSelected?.Invoke(0);
        } else if (camoGameObjectId == CamoGameObject2.GetInstanceID()) {
            OnCamoSelected?.Invoke(1);
        } else if (camoGameObjectId == CamoGameObject3.GetInstanceID()) {
            OnCamoSelected?.Invoke(2);
        } else if (camoGameObjectId == CamoGameObject4.GetInstanceID()) {
            OnCamoSelected?.Invoke(3);
        } else if (camoGameObjectId == CamoGameObject5.GetInstanceID()) {
            OnCamoSelected?.Invoke(4);
        } else if (camoGameObjectId == CamoGameObject6.GetInstanceID()) {
            OnCamoSelected?.Invoke(5);
        } else if (camoGameObjectId == CamoGameObject7.GetInstanceID()) {
            OnCamoSelected?.Invoke(6);
        }
    }

    private void ApplyCamoOnSelectedSpaceShip(int camoIndex)
    {
        ApplyCamo(currentSelectedSpaceShip, camoIndex);
    }

    private void SaveCamoOnSelectedSpaceShip(int camoIndex)
    {
        string selectedSpaceShipId = playerSpaceShips[currentSelectedSpaceShip].id;
        storeRepository.SelectCamoOnSpaceShip(selectedSpaceShipId, camoIndex);
    }

    private void ApplyCamo(int spaceShipIndex, int camoIndex)
    {
        switch (camoIndex)
        {
            case 0:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial1; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial1; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial1; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial1; break;
                }
                break;
            case 1:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial2; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial2; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial2; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial2; break;
                }
                break;
            case 2:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial3; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial3; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial3; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial3; break;
                }
                break;
            case 3:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial4; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial4; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial4; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial4; break;
                }
                break;
            case 4:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial5; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial5; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial5; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial5; break;
                }
                break;
            case 5:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial6; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial6; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial6; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial6; break;
                }
                break;
            case 6:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial7; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial7; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial7; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial7; break;
                }
                break;

        }
    }
    public void BuySpaceShip(int spaceShipIndex)
    {
        storeRepository.PurchaseNewSpaceShip(playerSpaceShips[spaceShipIndex].id, playerSpaceShips[spaceShipIndex].price);
        OnEnable();
    }

    private int currentlyBuyingSpaceShip = -1;

    public void OnSelectBuySpaceShip(int index)
    {
        if (playerSpaceShips[index].isUnlocked)
        {
            UpdateSpaceShipRelatedUI();
            OnSpaceShipSelected?.Invoke(index);
        }
        else
        {
            BuySpaceShipPanel.SetActive(true);
            currentlyBuyingSpaceShip = index;
        }
    }

    public void OnBuySpaceShipConfirm()
    {
        if(IsEnoughMoney(playerSpaceShips[currentlyBuyingSpaceShip].price))
        {
            BuySpaceShip(currentlyBuyingSpaceShip);
            currentlyBuyingSpaceShip = -1;
            BuySpaceShipPanel.SetActive(false);
        }
        else        
            DisplayNoEnoughCoinsDialog();
    }

    public void DisplayNoEnoughCoinsDialog() { NoEnoughMoneyDialogPanel.SetActive(true); }

    public void OnConfirmNoCoinsDialog() { NoEnoughMoneyDialogPanel.SetActive(false); }

    public void OnBuySpaceShipDecline()
    {
        currentlyBuyingSpaceShip = -1;
        BuySpaceShipPanel.SetActive(false);
    }

    private bool IsEnoughMoney(int price)
    {
        int currentPlayerCoins = storeRepository.GetPlayerCoins();
        return currentPlayerCoins >= price;
    }

    private void UpdateCoins()
    {
        int coins = storeRepository.GetPlayerCoins();
        coinsText.text = $"${coins}";
    }

}
