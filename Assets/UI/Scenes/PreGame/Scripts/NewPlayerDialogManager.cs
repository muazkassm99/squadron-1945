﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NewPlayerDialogManager : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField nameInputField;

    [SerializeField]
    private GameObject ChoosePlayerGameObject;

    private void Start() {

    }

    public void OnConfirm() {
        if(nameInputField.text != "" || nameInputField.text != null) {
            ChoosePlayerGameObject.GetComponent<ChoosePlayerMenuManager>().CreateNewPlayer(nameInputField.text);
        }
    }
}
