﻿using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Constants;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using Space_Squadron.Assets.Data.Repositories.Settings;
using Space_Squadron.Assets.Data.Entities;
using System;
using UnityEngine.EventSystems;

public class PreGameSceneManager : MonoBehaviour
{

    [SerializeField]
    private GameObject IntroMenu;

    [SerializeField]
    private GameObject ChoosePlayerMenu;

    [SerializeField]
    private GameObject NewPlayerDialog;

    [SerializeField]
    private GameObject NavigationMenu;

    [SerializeField]
    private GameObject SettingsMenu;

    [SerializeField]
    private GameObject StoreMenu;

    [SerializeField]
    private AudioMixer mixer;

    private SettingsRepository settingsRespository;

    private void Start() {

        AddPhysicsRayCaster();

        settingsRespository = SettingsRepositoryImpl.GetInstance();

        MenuEvents.instance.onPlay += transitionFromIntroToChoosePlayer;

        MenuEvents.instance.onGameExit += endGame;

        MenuEvents.instance.onNewPlayer += ShowNewPlayerDialog;

        MenuEvents.instance.onPlayerChosen += TransitionFromChoosePlayerToNavigation;
        MenuEvents.instance.onPlayerChosen += ApplyChosenPlayerSettings;

        MenuEvents.instance.onSettingsApply += TransitionFromSettingsToNavigation;

        MenuEvents.instance.onSettingsCanceled += ApplyChosenPlayerSettings;
        MenuEvents.instance.onSettingsCanceled += TransitionFromSettingsToNavigation;
    }

    private void AddPhysicsRayCaster()
    {
        PhysicsRaycaster physicsRaycaster = GameObject.FindObjectOfType<PhysicsRaycaster>();
        if (physicsRaycaster == null)
        {
            Camera.main.gameObject.AddComponent<PhysicsRaycaster>();
        }
    }

    public void transitionFromIntroToChoosePlayer() {
        IntroMenu.SetActive(false);
        ChoosePlayerMenu.SetActive(true);
    }

    public void TransitionFromChoosePlayerToIntro() {
        ChoosePlayerMenu.SetActive(false);
        IntroMenu.SetActive(true);
    }

    public void endGame() {
        Application.Quit();
    }

    public void ShowNewPlayerDialog() {
        NewPlayerDialog.SetActive(true);
    }

    public void HideShowNewPlayerDialog() {
        NewPlayerDialog.SetActive(false);
    }

    public void TransitionFromChoosePlayerToNavigation() {
        Debug.Log("player has been chosen and the name is " + PlayerPrefs.GetString(PlayerPrefsKeys.CURRENT_PLAYER_NAME) );
        NewPlayerDialog.SetActive(false);
        ChoosePlayerMenu.SetActive(false);
        NavigationMenu.SetActive(true);
    }

    public void ApplyChosenPlayerSettings() {
        //todo: move repository logic to the settings menu manager, specificly to OnDisable Callback method.
        SettingsEntity settings = settingsRespository.GetCurrentPlayerSettings();
        settingsRespository.ApplySettings(settings);
        mixer.SetFloat("Volume", settings.soundVolume);
    }

    public void TransitionFromNavigationToSettings() {
        NavigationMenu.SetActive(false);
        SettingsMenu.SetActive(true);
    }

    public void TransitionFromSettingsToNavigation() {
        SettingsMenu.SetActive(false);
        NavigationMenu.SetActive(true);
    }

    public void TransitionFromNavigationToChoosePlayer() {
        NavigationMenu.SetActive(false);
        ChoosePlayerMenu.SetActive(true);   
    }

    public void TransitionToScene1()
    {
        SceneManager.LoadScene(1);
    }

    public void TransitionFromNavigationToStore()
    {
        NavigationMenu.SetActive(false);
        StoreMenu.SetActive(true);
    }

    public void TransitionFromStoreToNavigation()
    {
        StoreMenu.SetActive(false);
        NavigationMenu.SetActive(true);
    }
}
