﻿using System;
using System.Dynamic;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Repositories.Settings;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class SettingsMenuManager : MonoBehaviour
{

    [SerializeField]
    private Button decreaseDisplayResolutionButton;

    [SerializeField]
    private Button increaseDisplayResolutionButton;

    private int ScreenResolutionChoice;

    [SerializeField]
    private TMP_Text DisplayResolutionText;

    private IList<string> screenResolutions;

    //////////////////////////////////////////////////////
    
    [SerializeField]
    private Button decreaseGraphicsQualityButton;

    [SerializeField]
    private Button increaseGraphicsQualityButton;

    private int graphicsQualityChoice;

    [SerializeField]
    private TMP_Text graphicsQualityText;

    private IList<string> graphicsQualities;

    //////////////////////////////////////////////////////
    
    [SerializeField]
    private Button decreaseControlMethodButton;

    [SerializeField]
    private Button increaseControlMethodButton;

    private int controlMethodChoice;

    [SerializeField]
    private TMP_Text controlMethodText;

    private IList<string> controlMethods;

    //////////////////////////////////////////////////////

    [SerializeField]
    private Slider volumeSlider;

    private float volumeChoice;

    //////////////////////////////////////////////////////
    
    [SerializeField]
    private Button decreaseGameDifficultyButton;

    [SerializeField]
    private Button increaseGameDifficultyButton;
    
    private int gameDifficultyChoice;

    [SerializeField]
    private TMP_Text gameDifficultyText;

    private IList<string> difficulties;

    //////////////////////////////////////////////////////

    [SerializeField]
    private Button apply;

    public AudioMixer mixer;

    //////////////////////////////////////////////////////

    private SettingsRepository settingsRepository;

    private SettingsEntity settings;

    private void Awake() {
        settingsRepository = SettingsRepositoryImpl.GetInstance();

        screenResolutions = new List<string>();
        screenResolutions.Add("1920x1080");
        screenResolutions.Add("1280x720");

        graphicsQualities = new List<string>();
        graphicsQualities.Add("Low");
        graphicsQualities.Add("Medium");
        graphicsQualities.Add("High");

        controlMethods = new List<string>();
        controlMethods.Add("M&KB");
        controlMethods.Add("Gamepad");

        difficulties = new List<string>();
        difficulties.Add("Easy");
        difficulties.Add("Medium");
        difficulties.Add("Hard");
    }

    private void Start() {
        MenuEvents.instance.onSettingsApply += ApplySettings;
    }

    public void ApplySettings() {
        string[] res = screenResolutions[ScreenResolutionChoice].Split('x');
        int width = Convert.ToInt32(res[0]);
        int height = Convert.ToInt32(res[1]);
        SettingsEntity created = new SettingsEntity() {
            id = settings.id,
            displayResolutionWidth = width,
            displayResolutionHeight = height,
            graphicsQuality = graphicsQualityChoice,
            controlType = controlMethodChoice,
            soundVolume = volumeSlider.value,
            gameDifficulty = gameDifficultyChoice
        };
        settingsRepository.ApplySettings(created);
    }

    void OnEnable() {
        Debug.Log("settings enabled");
        settings = settingsRepository.GetCurrentPlayerSettings();

        string width = Convert.ToString(settings.displayResolutionWidth);
        string height = Convert.ToString(settings.displayResolutionHeight);
        string res = width + 'x' + height;

        for(int i = 0; i < screenResolutions.Count; i++) {
            if(res.Equals(screenResolutions[i])) {
                ScreenResolutionChoice = i;
            }
        }

        graphicsQualityChoice = settings.graphicsQuality;
        controlMethodChoice = settings.controlType;
        volumeChoice = settings.soundVolume;
        gameDifficultyChoice = settings.gameDifficulty;

        Debug.Log(graphicsQualityChoice);
        Debug.Log(controlMethodChoice);
        Debug.Log(volumeChoice);
        Debug.Log(gameDifficultyChoice);
        
        DisplayResolutionText.text = screenResolutions[ScreenResolutionChoice];
        graphicsQualityText.text = graphicsQualities[graphicsQualityChoice];
        controlMethodText.text = controlMethods[controlMethodChoice];
        gameDifficultyText.text = difficulties[gameDifficultyChoice];
        volumeSlider.value = settings.soundVolume;

        apply.interactable = false;
    }

    public void SliderOnChange(float value) {
        volumeChoice = value;
        mixer.SetFloat("Volume", value);
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnDecreaseScreenRes() {
        if(ScreenResolutionChoice == 0) {
            ScreenResolutionChoice = screenResolutions.Count - 1;
        } else {
            ScreenResolutionChoice--;
        }
        DisplayResolutionText.text = screenResolutions[ScreenResolutionChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnIncreaseScreenRes() {
        if(ScreenResolutionChoice == screenResolutions.Count - 1) {
            ScreenResolutionChoice = 0;
        } else {
            ScreenResolutionChoice++;
        }
        DisplayResolutionText.text = screenResolutions[ScreenResolutionChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnDecreaseGraphicsQuality() {
        Debug.Log(graphicsQualityChoice);
        if(graphicsQualityChoice == 0) {
            graphicsQualityChoice = graphicsQualities.Count - 1;
        } else {
            graphicsQualityChoice--;
        }
        graphicsQualityText.text = graphicsQualities[graphicsQualityChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnIncreaseGraphicsQuality() {
        Debug.Log(graphicsQualityChoice);
        if(graphicsQualityChoice == graphicsQualities.Count - 1) {
            graphicsQualityChoice = 0;
        } else {
            graphicsQualityChoice++;
        }
        graphicsQualityText.text = graphicsQualities[graphicsQualityChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnDecreaseControl() {
        Debug.Log(controlMethodChoice);
        if(controlMethodChoice == 0) {
            controlMethodChoice = controlMethods.Count - 1;
        } else {
            controlMethodChoice--;
        }
        controlMethodText.text = controlMethods[controlMethodChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnIncreaseControl() {
        Debug.Log(controlMethodChoice);
        if(controlMethodChoice == controlMethods.Count - 1) {
            controlMethodChoice = 0;
        } else {
            controlMethodChoice++;
        }
        controlMethodText.text = controlMethods[controlMethodChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnDecreaseDifficulty() {
        Debug.Log(gameDifficultyChoice);
        if(gameDifficultyChoice == 0) {
            gameDifficultyChoice = difficulties.Count - 1;
        } else {
            gameDifficultyChoice--;
        }
        gameDifficultyText.text = difficulties[gameDifficultyChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnIncreaseDifficulty() {
        Debug.Log(gameDifficultyChoice);
        if(gameDifficultyChoice == difficulties.Count - 1) {
            gameDifficultyChoice = 0;
        } else {
            gameDifficultyChoice++;
        }
        gameDifficultyText.text = difficulties[gameDifficultyChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    private bool CanBeApplied() => 
        controlMethodChoice != settings.controlType ||
        gameDifficultyChoice != settings.gameDifficulty ||
        graphicsQualityChoice != settings.graphicsQuality ||
        volumeChoice != settings.soundVolume ||
        ScreenResolutionChoice != savedRes();

    private int savedRes() {
        string width = Convert.ToString(settings.displayResolutionWidth);
        string height = Convert.ToString(settings.displayResolutionHeight);
        string res = width + 'x' + height;

        for(int i = 0; i < screenResolutions.Count; i++) {
            if(res.Equals(screenResolutions[i])) {
                return i;
            }
        }
        return 0;
    }
    
}
