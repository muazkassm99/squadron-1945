﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalManager : MonoBehaviour
{

    public int stationsAlive;
    public GameObject portalPrefab;

    // Start is called before the first frame update
    void Start()
    {
        stationsAlive = GameObject.Find("Spawner").GetComponentInChildren<StationSpawner>().stations;

        GameEvents.instance.onStationDestruction += Instance_onStationDestruction;
        GameEvents.instance.onSpawnPortal += Instance_onSpawnPortal;
        GameEvents.instance.onStationSpawned += Instance_onStationSpawned;
    }


    private void Instance_onStationSpawned()
    {
        stationsAlive = GameObject.Find("Spawner").GetComponentInChildren<StationSpawner>().stations;
    }

    private void Instance_onSpawnPortal()
    {
        Debug.Log("Portal Shall be Spawned");
        Instantiate(portalPrefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 90, 0));
    }

    private void Instance_onStationDestruction(Vector3 pos)
    {
        stationsAlive -= 1;
        Debug.Log("Stations alive " + stationsAlive);
        if (stationsAlive == 0)
        {
            GameEvents.instance.SpawnPortal();
        }
    }
}
