﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialWeaponItem : Item
{
    public override string Name => "SpecialWeaponItem";

    public override void onPick(GameObject gameObject)
    {
        //hide item

        //add item to the player ship

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            return;
        }
        this.onPick(collision.gameObject);
    }

}
