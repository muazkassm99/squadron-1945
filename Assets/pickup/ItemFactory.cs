﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class ItemFactory : MonoBehaviour
{
    [SerializeField] private GameObject[] ItemsPrefabs = new GameObject[1];
    private Dictionary<string, GameObject> itemsByNames;


    private void Start()
    {

        itemsByNames = new Dictionary<string, GameObject>();

        foreach (GameObject itemPrefab in ItemsPrefabs)
        {
            String itemName = itemPrefab.GetComponent<Item>().Name;
            itemsByNames.Add(itemName, itemPrefab);
        }
    }


    //Creates an item at a given position and the parent will be set too.
    public void CreateItem(string itemName, Vector3 position, Transform parent)
    {

        if (itemsByNames.ContainsKey(itemName))
        {
            Instantiate(itemsByNames[itemName], position, Quaternion.identity, parent);
        }
    }

    //Gets all the names of the items in the prefab array.
    public IEnumerable<String> GetItemsNames()
    {
        return itemsByNames.Keys;
    }
}
