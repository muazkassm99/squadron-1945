﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public abstract string Name { get; }
    public abstract void onPick(GameObject gameObject);
}
