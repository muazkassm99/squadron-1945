﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class ScoreItem : Item
{
    public override string Name => "ScoreItem";

    int scoreValue = 500;

    public override void onPick(GameObject gameObject)
    {
        //Fire an event of a Score Change.
        GameEvents.instance.ScoreChange(scoreValue);

        //Destroy Current GameObject.
        GameObject.Destroy(this.gameObject);
    }

    // Start is called before the first frame update
    void Awake()
    {
        //Get the value of the scoreChange variable form the repo.
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            this.onPick(gameObject);
        }     
    }
}
