﻿using UnityEngine;

public interface IPickable
{
    void onPick(GameObject gameObject);
}
