﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldItem : Item
{
    public override string Name => "ShieldItem";

    public override void onPick(GameObject gameObject)
    {
        //hide shield item

        //get player script of shield

        //increment sheild by some value

        //destroy game object
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            return;
        }
        this.onPick(collision.gameObject);
    }
}
