﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItem : Item
{

    [SerializeField] private float healthIncrement = 25f;

    public override string Name => "HealthItem";

    //TODO : Take value from Repo


    public override void onPick(GameObject other)
    {

        //Increase health by some value
        other.GetComponent<PlayerHandler>().Heal(healthIncrement);

        //destroy that game object
        GameObject.Destroy(gameObject);

    }

    
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Hit SomeThing");
        if (other.gameObject.tag == "Player")
        {
            //Debug.Log("Hit a player");
            this.onPick(other.gameObject);
        }
    }

}
