﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationSpawner : MonoBehaviour
{
    public GameObject spaceStation;
    public int stations;
    private Vector3[] positions;

    // Start is called before the first frame update
    void Start()
    {
        setPositions();

        for (int stationCounter = 0; stationCounter < stations; stationCounter++)
        {
            GameObject stationObj = Instantiate(spaceStation);
            stationObj.transform.position = positions[stationCounter];
        }

        GameEvents.instance.StationSpawned();
    }

    //spawn staions in the static positions.
    private void setPositions()
    {
        /// read this form file later
        stations = 4;
        positions = new Vector3[stations];
        positions[0] = new Vector3(-1800, 0, 0);
        positions[1] = new Vector3(1800, 0, 0);
        positions[2] = new Vector3(0, 0, -1800);
        positions[3] = new Vector3(0, 0, 1800);
    }
}
