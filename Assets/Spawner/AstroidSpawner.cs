﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AstroidSpawner : MonoBehaviour
{
    public GameObject[] prefabs = new GameObject[3];
    public Vector3 spawnBoundries = new Vector3(1000, 1000, 1000);
    public int objects;

    public int minScale = 5;
    public int maxSclae = 10;

    private void Awake()
    {
        //Load assets
    }

    // Start is called before the first frame update
    void Start()
    {
        setUp();
        
        for (int objsCounter = 0; objsCounter < objects; objsCounter++)
        {
            Vector3 spawnPosition = new Vector3(
                UnityEngine.Random.Range(-spawnBoundries.x, spawnBoundries.x),
                UnityEngine.Random.Range(-spawnBoundries.y, spawnBoundries.y),
                UnityEngine.Random.Range(-spawnBoundries.z, spawnBoundries.z)
                );
            int index = UnityEngine.Random.Range(0, prefabs.Length);
            GameObject astroid = Instantiate(prefabs[index]);
            astroid.transform.position = spawnPosition;
            int scale = UnityEngine.Random.Range(minScale, maxSclae);
            astroid.transform.localScale = new Vector3(scale, scale, scale);
        }
    }

    private void setUp()
    {
        /// read this from file later
        objects = 75;
    }
}
