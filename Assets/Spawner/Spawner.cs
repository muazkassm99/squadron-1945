﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject [] prefabs = new GameObject[2];
    public GameObject spaceStation;
    public Vector3 spawnBoundries;
    public Vector3 stationBoundries;
    public int objects;
    public int stations;
    public int minScale = 5;
    public int maxSclae = 10;
    
    private int objsCounter = 0;
    private int stationCounter = 0;






    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        while(objsCounter < objects)
        {
            Vector3 spawnPosition = new Vector3(
                UnityEngine.Random.Range(-spawnBoundries.x, spawnBoundries.x),
                UnityEngine.Random.Range(-spawnBoundries.y, spawnBoundries.y),
                UnityEngine.Random.Range(-spawnBoundries.z, spawnBoundries.z)
                );
            int index = UnityEngine.Random.Range(0, prefabs.Length);
            GameObject astroid = Instantiate(prefabs[index]);
            astroid.transform.position = spawnPosition;
            int scale = UnityEngine.Random.Range(minScale, maxSclae);
            astroid.transform.localScale = new Vector3(scale, scale, scale);
            objsCounter++;
        }

        while(stationCounter < stations)
        {
            Vector3 spawnPosition = new Vector3(
                UnityEngine.Random.Range(spawnBoundries.x, stationBoundries.x) * randomPosOrNeg(),
                UnityEngine.Random.Range(spawnBoundries.y, stationBoundries.y) * randomPosOrNeg(),
                UnityEngine.Random.Range(spawnBoundries.z, stationBoundries.z) * randomPosOrNeg()
                );

            GameObject stationObj = Instantiate(spaceStation);
            stationObj.transform.position = spawnPosition;
            stationCounter++;
        }
    }

    private int randomPosOrNeg()
    {
        return UnityEngine.Random.Range(0, 2) * 2 - 1;
    }
}
