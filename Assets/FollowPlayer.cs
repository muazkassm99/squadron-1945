﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    public Transform player;
    public Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(0, 7, -17);
        Cursor.lockState = CursorLockMode.Locked;
    }


    public float transitionSpeed = 0.5f;

    private void LateUpdate()
    {
/*        transform.position = player.position + offset;*/

        /*float x = Mathf.Lerp(transform.position.x, player.position.x, 0.5f);
        float y = Mathf.Lerp(transform.position.y, player.position.y, 0.5f);
        float z = Mathf.Lerp(transform.position.z, player.position.z, 0.5f);
        transform.localPosition = new Vector3(x, y, z) + offset;*/


        Vector3 currentAngle = new Vector3(
        Mathf.LerpAngle(transform.rotation.eulerAngles.x, player.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
        Mathf.LerpAngle(transform.rotation.eulerAngles.y, player.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
        Mathf.LerpAngle(transform.rotation.eulerAngles.z, player.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed));

        transform.eulerAngles = currentAngle;

    }
}
