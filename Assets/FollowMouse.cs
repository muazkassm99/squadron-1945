﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{

    public float followSpeed = 0.1f;


    Vector3 mousePos;
    CharacterController characterController;

    Vector3 position = new Vector3(0f, 0f, 0f);


    private void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        position = Vector3.Lerp(transform.position, mousePos, followSpeed);

        characterController.Move(position);
    }

}
