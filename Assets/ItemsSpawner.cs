﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemsSpawner : MonoBehaviour
{

    [SerializeField] private GameObject healthItemPrefab;
    [SerializeField] private GameObject shieldItemPrefab;

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.instance.onAstroidDestruction += Instance_onAstroidDestruction;
    }

    private void Instance_onAstroidDestruction(Vector3 pos)
    {
        int random = Random.Range(1, 101);
        //Debug.Log(random);
        //30% chance of spawning an Item
        if(random <= 30)
        {
            //The Factory will create an Item (a random one).
            gameObject.GetComponent<ItemFactory>().CreateItem(GenerateRandomItem(), pos, gameObject.transform);
        }


    }


    //Gets a random Item from all the item set in the Item Factory Class
    private string GenerateRandomItem()
    {
        IEnumerable<string>itemsNames = gameObject.GetComponent<ItemFactory>().GetItemsNames();
        string[] names = itemsNames.ToArray<string>();
        int rand = Random.Range(0, names.Length);
        //Debug.Log(names[rand]);
        return names[rand];

    }
}
