﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.instance.onLevelWin += Instance_onLevelWin;
        GameEvents.instance.onLevelLose += Instance_onLevelLose;
    }

    private void Instance_onLevelLose()
    {
        //reload level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Instance_onLevelWin()
    {
        //load next scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
