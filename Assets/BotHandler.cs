﻿using System;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities.GameData;
using UnityEngine;

public class BotHandler : Target
{

    //private float health = 500;
    private void Start()
    {

        setUp(); //sets up the initial values form the database.
        this.healthSystem = new HealthSystem(health);
        this.healthBar = new TargetHealthBar(this.healthSystem);
        this.healthSystem.onHealthChanged += HealthSystem_onHealthChanged;
    }


    //sets up the initial values form the database.
    private void setUp()
    {
        SceneEntity currentScene = DataManager.instance.GetSceneEntity();
        this.health = currentScene.enemyBots[0].hp;
        this.scoreValue = (int)currentScene.enemyBots[0].scoreValue;
        this.healthRegen = currentScene.enemyBots[0].healthRegen;
        this.shield = currentScene.enemyBots[0].shield;
    }

    public override void Die()
    {
        Vector3 pos = gameObject.transform.position;
        GameObject.Destroy(gameObject);
        GameEvents.instance.EnemyShipDestruction(pos);
        this.healthBar.healthBarObject.gameObject.transform.parent.gameObject.SetActive(false);
        GameEvents.instance.ScoreChange(this.scoreValue);
    }
}
