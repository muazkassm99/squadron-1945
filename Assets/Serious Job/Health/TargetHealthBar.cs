﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetHealthBar : HealthBar
{

    
    public TargetHealthBar(HealthSystem healthSystem) : base(healthSystem)
    {
        //set HealthBar object
        this.healthBarObject = GameObject.Find("PlayerCanvas").transform.Find("HealthBarTarget").GetComponentInChildren<Slider>();
        this.healthBarObject.value = 0.5f;

        
        //this.healthBarObject =  GameObject.Find("SomeCanvas").getComponentInChildren<Slider>();
    }

    public override void AttachEventHealthChange()
    {
        this.healthSystem.onHealthChanged += HealthSystem_onHealthChanged;
    }

    private void HealthSystem_onHealthChanged()
    {
        //this.healthBarObject.value = this.healthSystem.GetHealthPercentage();
    }

}
