﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : HealthBar
{

    public PlayerHealthBar(HealthSystem healthSystem) : base(healthSystem) {
        this.healthBarObject = GameObject.Find("PlayerCanvas").GetComponentInChildren<Slider>();
    }

    public override void AttachEventHealthChange()
    {
        healthSystem.onHealthChanged += HealthSystem_onHealthChanged;
    }

    private void HealthSystem_onHealthChanged()
    {
        healthBarObject.value = healthSystem.GetHealthPercentage();
        healthBarObject.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color =
                Color.Lerp(MinHealthColor, MaxHealthColor, this.healthSystem.GetHealthPercentage());
    }

}
