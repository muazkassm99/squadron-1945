﻿using System;
using UnityEngine;
using UnityEngine.UI;

abstract public class HealthBar : MonoBehaviour
{

    public Color MaxHealthColor = Color.green;
    public Color MinHealthColor = Color.red;

    //The health system which the health bar uses.
    protected HealthSystem healthSystem;

    //The HealthBarObject we may use (could be used for many sliders). TO TEST
    public Slider healthBarObject;
    public HealthBar(HealthSystem healthSystem)
    {
        this.healthSystem = healthSystem;
        //GameEvents.instance.onPlayerHealthChanged += Instance_onPlayerHealthChanged; //Attaching a function for that event.
        AttachEventHealthChange();
    }

    public abstract void AttachEventHealthChange();
}
