﻿using System;
using System.Diagnostics;
using UnityEngine;

/*
 *   This is a class of pure c#
 *   it represents the health (as attributes and functionality)
 *   where every object that has health can TakeDamage() and Heal() for some amount.
 *
 *    
*/
public class HealthSystem
{
    protected float health;
    protected float maxHealth;

    public event Action onHealthChanged;
    public event Action onDeathEvent;

    //A constructor that takes in the initial health value.
    public HealthSystem(float maxHealth)
    {
        this.maxHealth = maxHealth;
        this.health = maxHealth;
    }


    /*
     * This function represent the object taking damage.
     * and fires the event for health change.
     */
    public void TakeDamage(float amountOfDamage)
    {
        health -= amountOfDamage;
        onHealthChanged?.Invoke();

        if(health <= 0)
        {
            health = 0;
            onDeathEvent?.Invoke();
        }

        //GameEvents.instance.HealthChanged();
        //onHealthChanged?.Invoke(); 
    }


    /*
     * This function represent the object healing.
     * and fires the event for health change.
     */
    public void Heal(float amountOfHeal)
    {
        health += amountOfHeal;
        if(health > maxHealth)
        {
            health = maxHealth; 
        }
        //GameEvents.instance.HealthChanged();
        onHealthChanged?.Invoke();
    }


    //Getter for health
    public float GetHealth()
    {
        return this.health;
    }
    //Getter for health Precentage
    public float GetHealthPercentage()
    {
        return (float)this.GetHealth() / this.maxHealth;
    }


}
