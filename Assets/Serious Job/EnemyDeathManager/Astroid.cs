﻿using System;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities.GameData;
using UnityEngine;
using UnityEngine.UI;

public class Astroid : Target
{

    private void Start()
    {

        setUp();

        this.healthSystem = new HealthSystem(this.health);
        // /this.healthSystem = new HealthSystem(this.health);
        this.healthBar = new TargetHealthBar(this.healthSystem);
        
        //subscribing the base function to the child event.
        this.healthSystem.onHealthChanged += HealthSystem_onHealthChanged;
    }

    //this function gets the data from the repository.
    private void setUp()
    {
        
        SceneEntity currentScene = DataManager.instance.GetSceneEntity();
        this.health = currentScene.astroid.hp;
        this.scoreValue = (int)currentScene.astroid.scoreValue;
        this.shield = 0;
        this.healthRegen = 0;
    }


    public override void Die()
    {
        //when you die change distroy the curret object, hide the healthbar again and chaneg the score.
        Vector3 pos = gameObject.transform.position;
        GameObject.Destroy(gameObject);
        GameEvents.instance.AstroidDestruction(pos);
        this.healthBar.healthBarObject.gameObject.transform.parent.gameObject.SetActive(false);
        GameEvents.instance.ScoreChange(this.scoreValue);
    }

}
