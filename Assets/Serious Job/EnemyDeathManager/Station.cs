﻿using System;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities.GameData;
using UnityEngine;

public class Station : Target
{
    
    //private float health = 500;
    private void Start()
    {
        
        setUp(); //sets up the initial values form the database.
        this.healthSystem = new HealthSystem(health);
        this.healthBar = new TargetHealthBar(this.healthSystem);
        this.healthSystem.onHealthChanged += HealthSystem_onHealthChanged;
    }


    //sets up the initial values form the database.
    private void setUp()
    {
        SceneEntity currentScene = DataManager.instance.GetSceneEntity();
        this.health = currentScene.enemyStations[0].hp;
        this.scoreValue = (int)currentScene.enemyStations[0].score;
        this.healthRegen = currentScene.enemyStations[0].healthRegenerateRate;
        this.shield = 0;
    }

    public override void Die()
    {
        Vector3 pos = gameObject.transform.position;
        GameObject.Destroy(gameObject);
        GameEvents.instance.StationDestruction(pos);
        this.healthBar.healthBarObject.gameObject.transform.parent.gameObject.SetActive(false);
        GameEvents.instance.ScoreChange(this.scoreValue);
    }
}
