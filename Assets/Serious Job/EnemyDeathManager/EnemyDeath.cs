﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This Class is created to deal with shootable objects where each object
 * has its own value that can be added to the score.
 * 
 * Killing one of these objects will fire one of the  the Events shown in the Start()
 * and when one of these events fire it will cause of of the functions bellow to bt called
 * and when it does that .. the OnScoreChange event will fire in its turn..
 * 
 * */

public class EnemyDeath : MonoBehaviour
{

    public const int astroidScore = 20;
    public const int enemyShipScore = 50;
    public const int stationScore = 100;
    


    void Start()
    {
        // GameEvents.instance.onAstroidDestruction += Instance_onAstroidDestruction;
        // GameEvents.instance.onEnemyShipDestruction += Instance_onEnemyShipDestruction;
        // GameEvents.instance.onStationDestruction += Instance_onStationDestruction;
    }

    //Each object will fire OnScoreChange Event when it is destroied.

    private void Instance_onStationDestruction()
    {
        GameEvents.instance.ScoreChange(stationScore);
    }

    private void Instance_onEnemyShipDestruction()
    {
        GameEvents.instance.ScoreChange(enemyShipScore);
    }

    private void Instance_onAstroidDestruction()
    {
        GameEvents.instance.ScoreChange(astroidScore);
    }

}
