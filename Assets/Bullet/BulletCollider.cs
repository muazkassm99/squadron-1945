﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollider : MonoBehaviour
{
    public float dmg = 10f;

    private void OnTriggerEnter(Collider other)
    {
        Target target = other.GetComponent<Target>();
        if(target != null)
        { 
            Destroy(gameObject);

            other.GetComponent<Target>().Damage(dmg);
        }
    }
}
