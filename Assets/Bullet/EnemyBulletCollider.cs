﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletCollider : MonoBehaviour
{

    public float dmg = 10f;

    private void OnTriggerEnter(Collider other)
    {
        PlayerHandler player = other.GetComponent<PlayerHandler>();
        if (player != null)
        {
            Destroy(gameObject);

            other.GetComponent<PlayerHandler>().Damage(dmg);    
        }
    }
}
