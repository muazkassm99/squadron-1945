﻿using System;
using UnityEngine;

public class InputActions
{
    public event Action <Vector3> onTranslationsClick;
    public event Action <Vector3> onRotationsClick;
    public event Action onPrimaryShootingClick, onSecondaryShootingClick;
    public event Action onUsePrimarySpecialItemClick, onUseSecondarySpecialItemClick;
    public event Action onEnableTurboClick;
    public event Action onOpenMenuClick;

    public void TranslationsClick(Vector3 translationVector)
    {
        onTranslationsClick?.Invoke(translationVector);
    }
    public void RotationsClick(Vector3 rotationVector)
    {
        onRotationsClick?.Invoke(rotationVector);
    }

    public void PrimaryShootingClick()
    {
        onPrimaryShootingClick?.Invoke();
    }
    public void SecondaryShootingClick()
    {
        onSecondaryShootingClick?.Invoke();
    }

    public void UsePrimarySpecialItemClick()
    {
        onUsePrimarySpecialItemClick?.Invoke();
    }
    public void UseSecondarySpecialItemClick()
    {
        onUseSecondarySpecialItemClick?.Invoke();
    }

    public void EnableTurboClick()
    {
        onEnableTurboClick?.Invoke();
    }

    public void OpenMenuClick()
    {
        onOpenMenuClick?.Invoke();
    }
}
