﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuInputKeys
{
    public KeyCode upKey { set; get; }
    public KeyCode downKey { set; get; }
    public KeyCode leftKey { set; get; }
    public KeyCode rightKey { set; get; }
    public KeyCode selectKey { set; get; }
    public KeyCode backKey { set; get; }
    public KeyCode exitKey { set; get; }

    public MenuInputKeys() { }
    protected MenuInputKeys(KeyCode upKey, KeyCode downKey, KeyCode leftKey, KeyCode rightKey, KeyCode selectKey, KeyCode backKey, KeyCode exitKey)
    {
        this.upKey = upKey;
        this.downKey = downKey;
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.selectKey = selectKey;
        this.backKey = backKey;
        this.exitKey = exitKey;
    }
}
