﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDependence
{
    public TranslationKeys translationKeys;
    public RotationKeys rotationKeys;
    public KeyCode primaryShootingKey, altPrimaryShootingKey;
    public KeyCode secondaryShootingKey, altSecondaryShootingKey;
    public KeyCode usePrimarySpecialItemKey, useSecondarySpecialItemKey;
    public KeyCode enableTurbo;
    public KeyCode openMenu;


    public InputDependence() 
    { 
        translationKeys = new TranslationKeys();
        rotationKeys = new RotationKeys();
    }

    public InputDependence(TranslationKeys translationKeys, RotationKeys rotationKeys, 
                            KeyCode primaryShootingKey, KeyCode altPrimaryShootingKey, 
                            KeyCode secondaryShootingKey, KeyCode altSecondaryShootingKey, 
                            KeyCode usePrimarySpecialItemKey, KeyCode useSecondarySpecialItemKey, 
                            KeyCode enableTurbo, KeyCode openMenu)
    {
        this.translationKeys = translationKeys;
        this.rotationKeys = rotationKeys;
        this.primaryShootingKey = primaryShootingKey;
        this.altPrimaryShootingKey = altPrimaryShootingKey;
        this.secondaryShootingKey = secondaryShootingKey;
        this.altSecondaryShootingKey = altSecondaryShootingKey;
        this.usePrimarySpecialItemKey = usePrimarySpecialItemKey;
        this.useSecondarySpecialItemKey = useSecondarySpecialItemKey;
        this.enableTurbo = enableTurbo;
        this.openMenu = openMenu;
    }

    public bool IsTranslationKeysPressed()
    {
        return translationKeys.IsAnyKeyPressed();
    }
    public bool IsRotationKeysPressed()
    {
        return rotationKeys.IsAnyKeyPressed();
    }
    public bool IsPrimaryShootingKeyPressed()
    {
        return Input.GetKey(primaryShootingKey) || Input.GetKey(altPrimaryShootingKey);
    }
    public bool IsSecondaryShootingKeyPressed()
    {
        return Input.GetKey(secondaryShootingKey) || Input.GetKey(altSecondaryShootingKey);
    }
    public bool IsUsePrimarySpecialItemKeyPressed()
    {
        return Input.GetKey(usePrimarySpecialItemKey);
    }
    public bool IsUseSecondarySpecialItemKeyPressed()
    {
        return Input.GetKey(useSecondarySpecialItemKey);
    }
    public bool IsEnableTurboKeyPressed()
    {
        return Input.GetKey(enableTurbo);
    }
    public bool IsOpenMenuKeyPressed()
    {
        return Input.GetKeyDown(openMenu);
    }
}
