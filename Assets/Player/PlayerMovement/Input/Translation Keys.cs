﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationKeys
{
    public KeyCode forwardKey;
    public KeyCode backwardKey;
    public KeyCode leftwardKey;
    public KeyCode rightwardKey;
    public KeyCode upwardKey;
    public KeyCode downwardKey;

    public TranslationKeys() { }
    public TranslationKeys(KeyCode forwardKey, KeyCode backwardKey, KeyCode leftwardKey, KeyCode rightwardKey, KeyCode upwardKey, KeyCode downwardKey)
    {
        this.forwardKey = forwardKey;
        this.backwardKey = backwardKey;
        this.leftwardKey = leftwardKey;
        this.rightwardKey = rightwardKey;
        this.upwardKey = upwardKey;
        this.downwardKey = downwardKey;
    }

    public bool IsAnyKeyPressed()
    {
        return Input.GetKey(rightwardKey) || Input.GetKey(leftwardKey) || 
                Input.GetKey(upwardKey) || Input.GetKey(downwardKey) || 
                Input.GetKey(forwardKey) || Input.GetKey(backwardKey);
    }

    public Vector3 GetTranslationVector()
    {
        return new Vector3((Input.GetKey(rightwardKey) ? +1 : 0) - (Input.GetKey(leftwardKey) ? +1 : 0),
                            (Input.GetKey(upwardKey) ? +1 : 0) - (Input.GetKey(downwardKey) ? +1 : 0), 
                            (Input.GetKey(forwardKey) ? +1 : 0) - (Input.GetKey(backwardKey) ? +1 : 0));
    }    
}