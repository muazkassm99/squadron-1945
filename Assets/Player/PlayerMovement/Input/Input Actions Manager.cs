﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputActionsManager : MonoBehaviour
{
    public InputActions inputActions;
    public InputDependence inputDependence;

    void FixedUpdate()
    {
        if(inputDependence.translationKeys.IsAnyKeyPressed())
        {
            inputActions.TranslationsClick(inputDependence.translationKeys.GetTranslationVector());
        }

        if(inputDependence.rotationKeys.IsAnyKeyPressed())
        {
            inputActions.RotationsClick(inputDependence.rotationKeys.GetRotationVector());
        }

        if(inputDependence.IsPrimaryShootingKeyPressed())
        {
            inputActions.PrimaryShootingClick();
        }

        if(inputDependence.IsSecondaryShootingKeyPressed())
        {
            inputActions.SecondaryShootingClick();
        }

        if(inputDependence.IsUsePrimarySpecialItemKeyPressed())
        {
            inputActions.UsePrimarySpecialItemClick();
        }

        if(inputDependence.IsUseSecondarySpecialItemKeyPressed())
        {
            inputActions.UseSecondarySpecialItemClick();
        }

        if(inputDependence.IsEnableTurboKeyPressed())
        {
            inputActions.EnableTurboClick();
        }

        if(inputDependence.IsOpenMenuKeyPressed())
        {
            inputActions.OpenMenuClick();
        }
    }
}
