﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingInputKeys
{
    public TranslationKeys translationKeys;
    public RotationKeys rotationKeys;
    public ShootingKeys shootingKeys;
    public UseSpecialItemKeys useSpecialItemKeys;
    public KeyCode enableTurbo;
    public KeyCode openMenu;

    public PlayingInputKeys() 
    { 
        translationKeys = new TranslationKeys(KeyCode.W, KeyCode.S, default, default, KeyCode.Q, KeyCode.Z);
        rotationKeys = new RotationKeys(KeyCode.DownArrow, KeyCode.UpArrow, KeyCode.RightArrow, KeyCode.LeftArrow, KeyCode.A, KeyCode.D);
        // shootingKeys = new ShootingKeys(KeyCode.Space, default, default); unused 
        enableTurbo =  KeyCode.LeftShift;
    }
    public PlayingInputKeys(TranslationKeys translationKeys, RotationKeys rotationKeys, ShootingKeys shootingKeys, UseSpecialItemKeys useSpecialItemKeys, KeyCode openMenu)
    {
        this.translationKeys = translationKeys;
        this.rotationKeys = rotationKeys;
        this.shootingKeys = shootingKeys;
        this.useSpecialItemKeys = useSpecialItemKeys;
        this.openMenu = openMenu;
    }
}
