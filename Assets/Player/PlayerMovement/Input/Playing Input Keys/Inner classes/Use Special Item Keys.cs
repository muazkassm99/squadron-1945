﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseSpecialItemKeys
{
    public KeyCode useSpecialItem1Key { set; get; }
    public KeyCode useSpecialItem2Key { set; get; }
    public KeyCode useSpecialItem3Key { set; get; }

    public UseSpecialItemKeys() { }
    public UseSpecialItemKeys(KeyCode useSpecialItem1Key, KeyCode useSpecialItem2Key, KeyCode useSpecialItem3Key)
    {
        this.useSpecialItem1Key = useSpecialItem1Key;
        this.useSpecialItem2Key = useSpecialItem2Key;
        this.useSpecialItem3Key = useSpecialItem3Key;
    }
}