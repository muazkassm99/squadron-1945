﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingKeys
{
    public KeyCode shooting1Key { set; get; }
    public KeyCode shooting2Key { set; get; }
    public KeyCode shooting3Key { set; get; }

    ShootingKeys() { }
    public ShootingKeys(KeyCode shooting1Key, KeyCode shooting2Key, KeyCode shooting3Key)
    {
        this.shooting1Key = shooting1Key;
        this.shooting2Key = shooting2Key;
        this.shooting3Key = shooting3Key;
    }
}