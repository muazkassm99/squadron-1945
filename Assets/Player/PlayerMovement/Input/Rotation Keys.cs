﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationKeys
{
    public KeyCode rotateXClockwiseKey;
    public KeyCode rotateXCounterClockwiseKey;
    public KeyCode rotateYClockwiseKey;
    public KeyCode rotateYCounterClockwiseKey;
    public KeyCode rotateZClockwiseKey;
    public KeyCode rotateZCounterClockwiseKey;
    public string rotateXAxis;
    public string rotateYAxis;
    public string rotateZAxis;

    public RotationKeys() { }

    public RotationKeys(KeyCode rotateXClockwiseKey, KeyCode rotateXCounterClockwiseKey, 
                        KeyCode rotateYClockwiseKey, KeyCode rotateYCounterClockwiseKey, 
                        KeyCode rotateZClockwiseKey, KeyCode rotateZCounterClockwiseKey, 
                        string rotateXAxis = null, string rotateYAxis = null, string rotateZAxis = null)
    {
        this.rotateXClockwiseKey = rotateXClockwiseKey;
        this.rotateXCounterClockwiseKey = rotateXCounterClockwiseKey;
        this.rotateYClockwiseKey = rotateYClockwiseKey;
        this.rotateYCounterClockwiseKey = rotateYCounterClockwiseKey;
        this.rotateZClockwiseKey = rotateZClockwiseKey;
        this.rotateZCounterClockwiseKey = rotateZCounterClockwiseKey;
        this.rotateXAxis = rotateXAxis;
        this.rotateYAxis = rotateYAxis;
        this.rotateZAxis = rotateZAxis;
    }

    private bool IsAnyKeyPressed1()
    {
        return Input.GetKey(rotateXClockwiseKey) || Input.GetKey(rotateXCounterClockwiseKey) || 
                Input.GetKey(rotateYClockwiseKey) || Input.GetKey(rotateYCounterClockwiseKey) || 
                Input.GetKey(rotateZClockwiseKey) || Input.GetKey(rotateZCounterClockwiseKey);
    }

    private bool IsAnyKeyPressed2()
    {
        return Mathf.Abs(Input.GetAxis(rotateXAxis)) > 0.1f ||
                Mathf.Abs(Input.GetAxis(rotateYAxis)) > 0.1f || 
                Mathf.Abs(Input.GetAxis(rotateZAxis)) > 0.1f;
    }

    public bool IsAnyKeyPressed()
    {
        return IsAnyKeyPressed1() || IsAnyKeyPressed2();
    }

    public Vector3 GetRotationVector()
    {
        return new Vector3((Input.GetKey(rotateXClockwiseKey) ? +1 : 0) - (Input.GetKey(rotateXCounterClockwiseKey) ? +1 : 0),
                                (Input.GetKey(rotateYClockwiseKey) ? +1 : 0) - (Input.GetKey(rotateYCounterClockwiseKey) ? +1 : 0),
                                (Input.GetKey(rotateZClockwiseKey) ? +1 : 0) - (Input.GetKey(rotateZCounterClockwiseKey) ? +1 : 0))
                                + new Vector3(-Input.GetAxis(rotateXAxis), Input.GetAxis(rotateYAxis), Input.GetAxis(rotateZAxis));
    }
}
