﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateMove : BaseMoveUtil
{
    Engine movingObjectEngine;
    private Vector3 translationVector;

    public TranslateMove(Engine movingObjectEngine, Vector3 translationVector)
    {
        this.movingObjectEngine = movingObjectEngine;
        this.translationVector = translationVector;
    }

    public override void PerformMove()
    {
        movingObjectEngine.AcceptTranslation(translationVector.normalized);
    }
}
