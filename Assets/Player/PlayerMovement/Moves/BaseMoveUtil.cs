using UnityEngine;

public abstract class BaseMoveUtil
{
    public abstract void PerformMove();
}