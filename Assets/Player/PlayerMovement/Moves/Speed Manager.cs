﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedManager : MonoBehaviour
{
    // TODO : this stuff is useless because the speed is multiplied by a zero Vector
    private float inputSpeed;
    private float dInputSpeed;
    public float minSpeed;
    public float maxSpeed;
    public float speed
    {
        get
        {
            return Mathf.Lerp(minSpeed, maxSpeed, inputSpeed);
        }
    }

    public float speedPercentage
    {
        get
        {
            return (speed - minSpeed) / (maxSpeed - minSpeed);
        }
    }

    void Start()
    {
        inputSpeed = 0f;
        dInputSpeed = 4f;
    }

    void Update()
    {
        inputSpeed -= Time.deltaTime * dInputSpeed / 3f;
        inputSpeed = Mathf.Clamp01(inputSpeed);
    }

    public void AcceptAcceleration(float acceleration = 1f)
    {
        inputSpeed += Time.deltaTime * acceleration * dInputSpeed;
        inputSpeed = Mathf.Clamp01(inputSpeed);
    }
}
