﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEngine : MonoBehaviour
{
    public float minForceScale;
    public float maxForceScale;
    float forceSpeed;
    float dForceSpeed;
    public float forceScale
    {
        get
        {
            return Mathf.Lerp(minForceScale, maxForceScale, forceSpeed);
        }
    }
    public float rotationSpeed;

    void Start()
    {
        minForceScale = 30f;
        maxForceScale = 60f;
        rotationSpeed = 50f;
        forceSpeed =  0f;
        dForceSpeed = 1f;
    }

    void Update()
    {
        forceSpeed -= Time.deltaTime * dForceSpeed / 2;
        forceSpeed = Mathf.Clamp01(forceSpeed);
    }

    public void AcceptForce(Vector3 force)
    {
        throw new System.NotImplementedException();
    }

    public void AcceptTorque(Vector3 torque)
    {
        throw new System.NotImplementedException();
    }

    public void AcceptTurbo()
    {
        forceSpeed += Time.deltaTime * dForceSpeed;
        forceSpeed = Mathf.Clamp01(forceSpeed);
    }

    public void AcceptTranslation(Vector3 translationAxis)
    {
        gameObject.GetComponent<CharacterController>().Move(translationAxis * forceScale * Time.deltaTime);
        // transform.position += translationAxis * forceScale * Time.deltaTime;
    }

    public void AcceptRotation(Vector3 rotationAxis)
    {
        transform.RotateAround(transform.position, rotationAxis, rotationSpeed * Time.deltaTime);
    }
}
