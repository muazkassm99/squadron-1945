﻿using System.Collections.Generic;
using UnityEngine;

public class MyCamera : MonoBehaviour
{
    [SerializeField]
    public GameObject person;
    public float largestTimeDistance;
    private Vector3 distance;
    private Queue <MyTransform> lazyUpdateTransform;

    void Start()
    {
        // largestTimeDistance = 0.08f;
        largestTimeDistance = 0.1f;
        distance = new Vector3(0, 10, -30);
        lazyUpdateTransform = new Queue<MyTransform>();
    }

    void FixedUpdate()
    {
        float currentTime = Time.time;
        Transform currentPersonTransform = person.GetComponent<Transform>();
        lazyUpdateTransform.Enqueue(new MyTransform(currentTime, currentPersonTransform));
        MyTransform previousPersonTransform = lazyUpdateTransform.Peek();
        
        float up = 0.25f * (Vector3.Distance(currentPersonTransform.position, transform.position) - distance.magnitude);
        transform.position = previousPersonTransform.position + distance.x * previousPersonTransform.right +
                                                                (distance.y + up) * previousPersonTransform.up +
                                                                distance.z * previousPersonTransform.forward;

        transform.rotation = Quaternion.LookRotation(currentPersonTransform.position + 1000 * currentPersonTransform.forward +
                                                up * currentPersonTransform.up - transform.position, previousPersonTransform.up);
        /*        
        float up = 0.4f * (Vector3.Distance(currentPersonTransform.position, transform.position) - distance.magnitude);
        transform.rotation = Quaternion.LookRotation(currentPersonTransform.position + 
                                                        (10 + up) * currentPersonTransform.up - transform.position, 
                                                        currentPersonTransform.up);
                                                        */

        if(currentTime - lazyUpdateTransform.Peek().time > largestTimeDistance)
            lazyUpdateTransform.Dequeue();
    }
}
