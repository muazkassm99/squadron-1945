﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWorldPlayerController : MonoBehaviour
{
    public Engine engine;
    public InputActionsManager inputActionsManager;
    
    private void SetUpInputActionsManager()
    {
        // TODO : Read values from file.
        inputActionsManager = gameObject.AddComponent<InputActionsManager>();
        inputActionsManager.inputActions = new InputActions();
        inputActionsManager.inputDependence = new InputDependence(
                            new TranslationKeys(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D, KeyCode.Q, KeyCode.Z), 
                            new RotationKeys(KeyCode.DownArrow, KeyCode.UpArrow, KeyCode.RightArrow, 
                            KeyCode.LeftArrow, KeyCode.Comma, KeyCode.Period, "Mouse Y", "Mouse X", "Mouse ScrollWheel"),
                            KeyCode.Space, KeyCode.Mouse0, KeyCode.V, KeyCode.Mouse1, default, default, 
                            KeyCode.LeftShift, KeyCode.Escape);
    }

    private void SetUpEngine()
    {
        // TODO : Read values from file.
        engine = gameObject.AddComponent<Engine>();
        engine.SetTranlationSpeedManger(0f, 75f);
        engine.SetRotationSpeedManger(0f, 50f);
        engine.SetTurboSpeedManger(0f, 30f);
    }
    
    void Awake()
    {
        SetUpEngine();
        SetUpInputActionsManager();
    }

    void Start()
    {
        inputActionsManager.inputActions.onTranslationsClick += PerformTranslation;
        inputActionsManager.inputActions.onRotationsClick += PerformRotation;
        inputActionsManager.inputActions.onEnableTurboClick += engine.AcceptTurbo;
    }

    private void PerformTranslation(Vector3 translationVector)
    {
        translationVector = translationVector.x * engine.transform.right + 
                            translationVector.y * engine.transform.up + 
                            translationVector.z * engine.transform.forward;
        new TranslateMove(engine, translationVector).PerformMove();
    }

    private void PerformRotation(Vector3 rotationVector)
    {
        new RotateMove(engine, rotationVector.x * engine.transform.right).PerformMove();
        new RotateMove(engine, rotationVector.y * engine.transform.up).PerformMove();
        new RotateMove(engine, rotationVector.z * engine.transform.forward).PerformMove();
    }
}
