﻿using UnityEngine;

public class PlayerAudioManager : MonoBehaviour
{
    private AudioSource engineAudioSource;
    private AudioSource turboAudioSource;
    private AudioSource musicAudioSource;
    public float gameSoundsVolume;  // read this value from settings
    public float gameMusicVolume;   // read this value from settings
    private Engine engine;
    string[] musicTracks;

    private AudioSource SetUpAudioSource(string path)
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>(path);
        audioSource.loop = true;
        audioSource.Play();
        return audioSource;
    }
    
    void Awake()
    {
        musicTracks = new string[] {"Origen - Dance Of The Clouds"};
        engineAudioSource = SetUpAudioSource("FlightWind");
        turboAudioSource = SetUpAudioSource("JetEngine");
        musicAudioSource = SetUpAudioSource(musicTracks[0]);
    }

    void Start()
    {
        gameSoundsVolume = 0.5f;
        gameMusicVolume = 0.5f;
        engine = gameObject.GetComponent<Engine>();
    }

    void Update()
    {
        engineAudioSource.volume = gameSoundsVolume * (0.25f + 0.75f * engine.translationSpeedManager.speedPercentage);
        turboAudioSource.volume = gameSoundsVolume * engine.turpoSpeedManager.speedPercentage *
                                                        engine.translationSpeedManager.speedPercentage;
    }
}
