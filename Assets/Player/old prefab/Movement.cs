﻿using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public CharacterController characterController;
    /*public float speed = 50f;
    public float normalSpeed = 50f;
    public float rotation = 25f;
    public float sprintSpeed = 140f;
*/

    [SerializeField]
    public float speed { get; set; }

    [SerializeField]
    public float normalSpeed { get; set; }

    [SerializeField]
    public float sprintSpeed { get; set; }

    private void Awake()
    {
        
    }

    private void Start()
    {

        SpaceShipEntity spaceShipEntity = DataManager.instance.GetPlayerSpaceShip();
        this.speed = spaceShipEntity.maxSpeed;
        this.normalSpeed = spaceShipEntity.maxSpeed;
        this.sprintSpeed = spaceShipEntity.sprintSpeed;
        Cursor.lockState = CursorLockMode.Locked;

    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKey(KeyCode.LeftShift))
        {
            speed = sprintSpeed;
        }
        else
        {
            speed = normalSpeed;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        characterController.Move(move * speed * Time.deltaTime);
    }
}
