﻿using Space_Squadron.Assets.Data.Entities;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private WeaponsContainer weapons; 

    void Start()
    {
        // weapons = transform.GetComponentInChildren<WeaponsContainer>();
        SpaceShipEntity spaceShipEntity = DataManager.instance.GetPlayerSpaceShip();
        weapons = gameObject.GetComponentInChildren<WeaponsContainer>();
        gameObject.GetComponent<InputActionsManager>().inputActions.onPrimaryShootingClick += weapons.FirePrimary;
        gameObject.GetComponent<InputActionsManager>().inputActions.onSecondaryShootingClick += weapons.FireSecondary;
    }
}
