﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public RectTransform arrowPrefab;

    // Start is called before the first frame update
    void Start()
    {
        var canvas = GameObject.Find("PlayerCanvas").transform;
        var arr = Instantiate(arrowPrefab, canvas);
        arr.position = new Vector2(Screen.width / 2, Screen.height / 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
