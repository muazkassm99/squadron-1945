﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*
 * We place this script on top of the objects 
 * we want to show an indicator to.
 */
public class ArrowOnMe : MonoBehaviour
{
    public RectTransform arrowPrefab;

    private RectTransform myArrow;

    private Transform player;

    private float minX, minY, maxX, maxY;

    // Start is called before the first frame update
    void Start()
    {
        setXandYvalues();

        player = GameObject.Find("Player").GetComponent<Transform>();
        var canvas = GameObject.Find("PlayerCanvas").transform;
        myArrow = Instantiate(arrowPrefab, canvas);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        //Vector3 pos = Camera.main.WorldToViewportPoint(this.transform.position);

        if (Vector3.Dot((this.transform.position - player.position).normalized, player.forward) > 0)
        {
            myArrow.gameObject.SetActive(true);

            pos.x = Mathf.Clamp(pos.x, minX, maxX);
            pos.y = Mathf.Clamp(pos.y, minY, maxY);

            float distance = Vector3.Distance(player.position, this.transform.position);
            myArrow.gameObject.GetComponentInChildren<Text>().text = distance.ToString("0") + " m";

            if (distance <= 20)
            {
                myArrow.gameObject.SetActive(false);
            }
            else
            {
                myArrow.gameObject.SetActive(true);
            }
        }
        else
        {
            //myArrow.gameObject.SetActive(false);
            
            pos.x = Mathf.Clamp(pos.x, minX, maxX);
            pos.y = Mathf.Clamp(pos.y, minY, maxY);
        }

        myArrow.position = pos;
    }

    private void setXandYvalues()
    {
        /*
            These values are used to keep the indicator on the screen where we get the min X and Y values
            of the indicator that it can remain visible on the screen.
         */

        minX = arrowPrefab.gameObject.GetComponentInChildren<Image>().GetPixelAdjustedRect().width / 2;
        maxX = Screen.width - minX;

        minY = arrowPrefab.gameObject.GetComponentInChildren<Image>().GetPixelAdjustedRect().height / 2;
        maxY = Screen.height - minY;
    }
}
