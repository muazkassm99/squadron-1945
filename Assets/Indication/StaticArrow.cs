﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticArrow : MonoBehaviour
{

    Vector3 position = new Vector3(1600, 0, 0);
    public RectTransform arrowPrefab;
    private RectTransform myArrow;

    // Start is called before the first frame update
    void Start()
    {
        var canvas = GameObject.Find("PlayerCanvas").transform;
        myArrow = Instantiate(arrowPrefab, canvas);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(position);
        myArrow.position = pos;
    }
}
