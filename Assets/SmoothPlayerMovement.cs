﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothPlayerMovement : MonoBehaviour
{

    public CharacterController characterController;
    public float startSpeed = 1;
    public float speed = 0f;
    public float speedLimit = 50f; // maximum normal-flying speed
    public float maxSpeed = 50f; // maximum flying speed could change when pressing L-SHIFT
    public float sprintSpeed = 250f; //speed when pressing left shift key
    public float acc = 10; //acceleration



    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

    }
    private void Update()
    {
        Move();
        Rotate();

    }

    void Move()
    {
        //gets input from keyboard WASD
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        //Checks if sprint is required
        if (Input.GetKey(KeyCode.LeftShift))
        {
            maxSpeed = sprintSpeed;
        }
        else // when it is not pressed set the max speed to 50
        {
            maxSpeed = speedLimit;
        }


        //Always try to get speed to be the same as max speed
        /*
            wheather the maxSpeed is the sprint speed or it is the reqular speed limit
            always try to get speed to equal maxSpeed (get it to equal the max available speed). 
         */
        if (speed != maxSpeed)
        {
            speed = Mathf.SmoothDamp(speed, maxSpeed, ref acc, 0.5f);
        }


        /*
            if the player is not inputing directions reduce speed to the starting speed.
         */
        if (x == 0 && z == 0) // Not Input is comingIn
        {
            speed = Mathf.SmoothDamp(speed, startSpeed, ref acc, 0.5f);
        }

        Vector3 move = transform.right * x + transform.forward * z; //get the direction we want to move in.


        characterController.Move(move * speed * Time.deltaTime);// move the player object
    }


    public float mouseSensitivity = 100f;

    float xRotation = 0;
    float yRotation = 0;

    float zRotation = 0;

    float rotSpeed = 10;

    void Rotate()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, +90f);

        yRotation += mouseX;
        yRotation = Mathf.Clamp(yRotation, -180, +180f);

        /*
        *      
        zRotation += yRotation / 60 * 10;*/
        zRotation = Mathf.SmoothDamp(zRotation, -yRotation / 60 * 10, ref rotSpeed, 1);


        zRotation = -(speed * yRotation) / 300;

        zRotation = Mathf.Clamp(zRotation, -30f, 30f);


        transform.localRotation = Quaternion.Euler(xRotation, yRotation, zRotation);

    }
}
