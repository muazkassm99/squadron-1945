﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
	private GameObject pathControl;

	// Use this for initialization
	void Start()
	{
		this.pathControl = GameObject.Find("Path Control");
	}

	// Update is called once per frame
	void Update()
	{

		//if out of camera, recycle this ground.
		Vector3 pos = Camera.main.WorldToViewportPoint(this.transform.position);
		if (pos.z < -50)
		{
			PathController con = pathControl.GetComponent<PathController>();
			con.recycle();
		}
	}
}
