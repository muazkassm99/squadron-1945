using UnityEngine;
using UnityEngine.Events;

public class ProjectileLauncher : MonoBehaviour, Weapon {

    public GameObject projectilePrefab;

    public Transform spawnPoint;
    
    public float timeGap = 0.5f;

    public UnityEvent onShoot;

    public float rotationSpeed = 0.01f;

    public TargetingSystem targetingSystem;

    private float lastFire;
    private Transform rootTransform;

    void Start()
    {
        lastFire = -Mathf.Infinity;
        rootTransform = transform.root;

        targetingSystem = gameObject.GetComponentInParent<TargetingSystem>();
    }

    void FixedUpdate()
    {
        if(targetingSystem.lockTarget)
        {   
            transform.forward = targetingSystem.target - transform.position;
        }
        else
        {
            transform.forward = Vector3.Lerp(transform.forward, targetingSystem.target - transform.position, 0.01f * Time.deltaTime);
        }
    }

    public void Shoot()
    {
        if(lastFire + timeGap < Time.time)
        {   
            GameObject projectileObject = GameObject.Instantiate(projectilePrefab, spawnPoint.position, spawnPoint.rotation);

            // Debug.Log(rootTransform.gameObject.name);
            projectileObject.GetComponent<Projectile>().SetSenderRootTransform(rootTransform.gameObject);

            projectileObject.transform.forward = targetingSystem.target - transform.position;

            lastFire = Time.time;
            if(onShoot != null)
                onShoot.Invoke();
        }
    }
}