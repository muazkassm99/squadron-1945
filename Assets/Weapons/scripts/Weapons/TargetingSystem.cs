using UnityEngine;

public class TargetingSystem : MonoBehaviour {
    public bool targetObjects = true;

    public float aimRange = 500f;
    public float aimAngle = 10f;
    public Vector3 target;

    public bool isTargeting = false;

    public bool lockTarget = false;
    private Transform lastTarget;
    private float startTargetingTime = 0;

    void FixedUpdate()
    {
        Transform targetTransform = TargetFinder.GetTarget(gameObject, aimRange, aimAngle);

        if(targetTransform != null)
        {
            if(lastTarget != null)
            {
                if(targetTransform != lastTarget)
                {
                    lockTarget = false;
                    startTargetingTime = Time.time;
                }
                else 
                {
                    if(startTargetingTime + 0.5 < Time.time)
                    {
                        lockTarget = true;
                    }
                }
            }
            else 
            {
                lockTarget = false;
                startTargetingTime = Time.time;
            }
            lastTarget = targetTransform;
            target = targetTransform.position;
            isTargeting = true;
        }
        else
        {
            lastTarget = null;
            lockTarget = false;
            target = transform.position + 1000 * transform.forward;
            isTargeting = false;
        }
    }
}