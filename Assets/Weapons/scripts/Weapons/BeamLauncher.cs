using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnBeamLevelSetEventHandler : UnityEvent<float> { };

public class BeamLauncher : MonoBehaviour, Weapon
{
    public LineRenderer beamLineRenderer;

    public BeamHitEffect beamHitEffect;

    public Transform beamSpawn;

    public float range = 1000;

    public float maxBeamLevel = 1;
    public float MaxBeamLevel { set { maxBeamLevel = value; } }

    public string beamColorShaderProperty = "_Color";

    public float rotationSpeed = 0.01f;
    public TargetingSystem targetingSystem;

    private BeamState currentBeamState = BeamState.Off;

    private float beamStateStartTime = 0;

    private float beamLevel = 0;

    private bool firing = false;

    public float beamFadeInTime = 0.15f;

    public AnimationCurve beamFadeInCurve = AnimationCurve.Linear(0, 0, 1, 1);

    public float beamFadeOutTime = 0.33f;

    public AnimationCurve beamFadeOutCurve = AnimationCurve.Linear(0, 1, 1, 0);

    public OnBeamLevelSetEventHandler onBeamLevelSet;

    private Transform rootTransform;

    private Vector3 beamForward;

    private bool shootThisFrame = false;

    public float damage = 20f;

    void Start()
    {
        damage = 20f;
        shootThisFrame = false;
        rootTransform = transform.root;

        beamFadeOutTime = 0.33f;

        SetBeamLevel(0);
        if (beamHitEffect != null)
        {
            beamHitEffect.SetActivation(false);
        }

        targetingSystem = gameObject.GetComponentInParent<TargetingSystem>();
    }

    // Set the beam state
    private void SetBeamState(BeamState newBeamState)
    {

        switch (newBeamState)
        {
            case BeamState.FadingIn:

                currentBeamState = BeamState.FadingIn;
                beamStateStartTime = Time.time - beamLevel * beamFadeInTime;    // Assume linear fade in/out
                break;

            case BeamState.FadingOut:

                currentBeamState = BeamState.FadingOut;
                beamStateStartTime = Time.time - (1 - beamLevel) * beamFadeOutTime;     // Assume linear fade in/out
                break;

            case BeamState.Sustaining:

                currentBeamState = BeamState.Sustaining;
                beamStateStartTime = Time.time;
                break;

            case BeamState.Off:

                currentBeamState = BeamState.Off;
                beamStateStartTime = Time.time;
                break;

        }
    }


    // Do a hit scan
    private bool DoHitScan()
    {

        // Raycast
        RaycastHit[] hits;

        Transform target = TargetFinder.GetTarget(gameObject, 500, 10);
        Vector3 forward = beamSpawn.forward;
        // beamForward = beamSpawn.forward;
        if(target != null)
            forward = (target.position - beamSpawn.position).normalized;

        // if(beamForward == Vector3.zero)
        //     beamForward = forward;
        // else
        //     beamForward = Vector3.Lerp(beamForward, forward, 6f * Time.deltaTime);

        hits = Physics.RaycastAll(beamSpawn.position, beamForward, range);
        System.Array.Sort(hits, (a, b) => a.distance.CompareTo(b.distance));    // Sort by distance

        for (int i = 0; i < hits.Length; ++i)
        {
            if (rootTransform == hits[i].collider.transform.root)
            {
                continue;
            }
            
            UpdateBeamPositions(beamSpawn.position, hits[i].point);

            OnHitDetected(hits[i]);

            return true;

        }

        UpdateBeamPositions(beamSpawn.position, beamSpawn.position + beamForward * range);

        OnHitNotDetected();

        return false;
    }


    private void OnHitDetected(RaycastHit hit)
    {

        // Update hit effect
        if (beamHitEffect != null)
        {
            beamHitEffect.SetActivation(true);
            beamHitEffect.OnHit(hit, transform.root.gameObject, damage * Time.deltaTime);
        }

    }


    private void OnHitNotDetected()
    {

        // Disable hit effect
        if (beamHitEffect != null) beamHitEffect.SetActivation(false);
    }


    private void UpdateBeamPositions(Vector3 start, Vector3 end)
    {
        beamLineRenderer.SetPosition(0, beamLineRenderer.transform.InverseTransformPoint(start));
        beamLineRenderer.SetPosition(1, beamLineRenderer.transform.InverseTransformPoint(end));
    }

    public void SetBeamLevel(float level)
    {

        beamLevel = Mathf.Clamp(level, 0, maxBeamLevel);
        
        // Set the color
        if (beamLineRenderer.material.HasProperty(beamColorShaderProperty))
        {
            Color c = beamLineRenderer.material.GetColor(beamColorShaderProperty);
            c.a = beamLevel;
            beamLineRenderer.material.SetColor(beamColorShaderProperty, c);
        }            

        // Update hit effect
        if (beamHitEffect != null)
        {
            beamHitEffect.SetLevel(beamLevel);
        }
        
        // Call event
        onBeamLevelSet.Invoke(beamLevel);
    }

    void LateUpdate()
    {
        if(!shootThisFrame)
        {
            StopTriggering();
        }
        shootThisFrame = false;
    }

    public void Shoot()
    {
        if (!firing)
        {
            SetBeamState(BeamState.FadingIn);
        }

        shootThisFrame = true;
        firing = true;
    }

    public void StopTriggering()
    {
        if (firing)
        {
            SetBeamState(BeamState.FadingOut);
        }

        beamForward = Vector3.zero;
        firing = false;
    }

    void FixedUpdate()
    {
        
        // Handle beam transitions
        switch (currentBeamState)
        {
            case BeamState.FadingIn:

                float fadeInAmount = (Time.time - beamStateStartTime) / beamFadeInTime;
                if (fadeInAmount > 1)
                {
                    SetBeamLevel(1);
                    SetBeamState(BeamState.Sustaining);
                }
                else
                {
                    SetBeamLevel(Mathf.Clamp(fadeInAmount, 0, 1));
                }
                break;

            case BeamState.FadingOut:

                float fadeOutAmount = (Time.time - beamStateStartTime) / beamFadeOutTime;
                if (fadeOutAmount > 1)
                {
                    SetBeamLevel(0);
                    SetBeamState(BeamState.Off);
                    if (beamHitEffect!= null) beamHitEffect.SetActivation(false);
                }
                else
                {
                    SetBeamLevel(Mathf.Clamp(1 - fadeOutAmount, 0, 1));
                }
                break;

            case BeamState.Sustaining:

                SetBeamLevel(1);
                
                break;

            case BeamState.Off:
                SetBeamLevel(0);
                break;

        }

        if (currentBeamState != BeamState.Off)
        {
            DoHitScan();
        }

        //transform.forward = Vector3.Lerp(transform.forward, targetingSystem.target - transform.position, 0.01f * Time.deltaTime);
        if(targetingSystem.lockTarget)
        {   
            transform.forward = targetingSystem.target - transform.position;
            beamForward = transform.forward;
        }
        else
        {
            transform.forward = Vector3.Lerp(transform.forward, targetingSystem.target - transform.position, 0.01f * Time.deltaTime);
            beamForward = transform.forward;
        }
    }
}