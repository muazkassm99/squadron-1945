using UnityEngine;

public class Missile : Projectile
{
    public float startFollowingTime = 0.1f;

    private Transform target;
    private float startTime;

    void Start()
    {

        Destroy(gameObject, lifeTime);

        target = TargetFinder.GetTarget(senderRootGameObject, 500f, 45f);

        startTime = Time.time;
    }

    void Update()
    {
        Move();
    }

    protected override void Move()
    {
        if(Time.time > startTime + startFollowingTime && target != null)
        {
            // Debug.Log(target.position);
            transform.forward = Vector3.Lerp(transform.forward, target.position - transform.position, rotationSpeed * Time.deltaTime);           
        }

        transform.position += transform.forward * speed * Time.deltaTime;
    }

}