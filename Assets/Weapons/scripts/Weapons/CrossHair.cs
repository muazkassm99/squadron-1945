﻿using UnityEngine;
using UnityEngine.UI;

public class CrossHair : MonoBehaviour
{
    public CrosshairTarget target;

    public Image crosshairImage;

    public float crosshairUpdateSpeed = 5f;

    public Camera camera;

    public Color defaultColor;
    public Color onTargetingColor;

    RectTransform crosshairRectTransform;
    void Start()
    {
        crosshairRectTransform = crosshairImage.GetComponent<RectTransform>();

        crosshairRectTransform.position = camera.WorldToScreenPoint(target.transform.position + target.transform.forward * 250);
        // crosshairRectTransform.sizeDelta = 50 * Vector2.one;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        crosshairRectTransform.position = camera.WorldToScreenPoint(target.transform.position + target.toTarget * 250);
        if(target.isTargeting)
        {
            crosshairImage.color = Color.Lerp(crosshairImage.color, onTargetingColor, crosshairUpdateSpeed * Time.deltaTime);
        }
        else 
        {
            crosshairImage.color = Color.Lerp(crosshairImage.color, defaultColor, crosshairUpdateSpeed * Time.deltaTime);
        }

        // Debug.Log(crosshairImage.color);
    }
}
