using UnityEngine;

public class BeamHitEffect : MonoBehaviour {

    public string effectsMaterialColorKey = "_Color";

    public float maxGlowSize = 6;

    public float maxGlowAlpha = 1;

    public float maxSparkSize = 3;

    public float maxSparkAlpha = 1;

    private Material glowMaterial;
    private Material sparkMaterial;

    public ParticleSystem glowParticleSystem;
    private ParticleSystem.MainModule glowParticleSystemMainModule;

    public ParticleSystem sparkParticleSystem;
    private ParticleSystem.MainModule sparkParticleSystemMainModule;

    private Transform cachedTransform;
    public Transform CachedTransform { get { return cachedTransform; } }

    void Awake()
    {
        // Collect all the materials
        if (glowParticleSystem != null)
        {
            glowParticleSystemMainModule = glowParticleSystem.main;
            glowMaterial = glowParticleSystem.GetComponent<ParticleSystemRenderer>().material;
        }

        if (sparkParticleSystem != null)
        {
            sparkParticleSystemMainModule = sparkParticleSystem.main;
            sparkMaterial = sparkParticleSystem.GetComponent<ParticleSystemRenderer>().material;
        }

        cachedTransform = transform;

        // Deactivate the effect at the start
        SetActivation(false);

    }

    public void SetLevel(float level)
    {

        if (glowParticleSystem != null)
        {
            glowParticleSystemMainModule.startSize = level * maxGlowSize;

            Color c = glowMaterial.GetColor(effectsMaterialColorKey);
            c.a = level;
            glowMaterial.SetColor(effectsMaterialColorKey, c);
        }

        if (sparkParticleSystem != null)
        {
            sparkParticleSystemMainModule.startSize = level * maxSparkSize;

            Color c = sparkMaterial.GetColor(effectsMaterialColorKey);
            c.a = level;
            sparkMaterial.SetColor(effectsMaterialColorKey, c);
        }
    }

    public void SetActivation(bool activate)
    {
        gameObject.SetActive(activate);
    }

    public void OnHit(RaycastHit hit, GameObject sender, float damage)
    {
        SetActivation(true);
        transform.position = hit.point;
        transform.rotation = Quaternion.LookRotation(hit.normal);

        DamageDealer.dealDamage(sender, hit.collider.gameObject, damage);
    }
}