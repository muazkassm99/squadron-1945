using UnityEngine;
using System.Collections.Generic;
public class WeaponsContainer : MonoBehaviour {
    public GameObject projectileLauncherPrefab;
    public GameObject laserBeamPrefab;
    public GameObject missileLauncherPrefab;

    public List<Transform> primaryWeaponsTransforms;
    public List<Transform> secondaryWeaponsTransforms;

    public List<Weapon> primaryWeapons;
    public List<Weapon> secondaryWeapons;

    void Start()
    {
        primaryWeapons = new List<Weapon>();
        secondaryWeapons = new List<Weapon>();

        for(int i = 0; i < primaryWeaponsTransforms.Count; i++)
        {
            primaryWeapons.Add(primaryWeaponsTransforms[i].GetComponent<Weapon>());
        }

        for(int i = 0; i < secondaryWeaponsTransforms.Count; i++)
        {
            secondaryWeapons.Add(secondaryWeaponsTransforms[i].GetComponent<Weapon>());
        }
    }

    public void FirePrimary()
    {
        for(int i = 0; i < primaryWeapons.Count; i++)
        {
            if(primaryWeapons[i] != null)
                primaryWeapons[i].Shoot();
        }
    }

    public void FireSecondary()
    {
        for(int i = 0; i < secondaryWeapons.Count; i++)
        {
            if(secondaryWeapons[i] != null)
                secondaryWeapons[i].Shoot();
        }
    }
}