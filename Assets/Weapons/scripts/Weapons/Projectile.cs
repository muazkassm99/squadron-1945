using UnityEngine;

public class Projectile : MonoBehaviour {
    public float damage;
    public float lifeTime;
    public float speed;
    public float rotationSpeed = 0.1f;

    public GameObject hitEffectPrefab;

    protected GameObject senderRootGameObject;

    void Start()
    {
        Destroy(gameObject, lifeTime);
    }

    void Update()
    {
        Move();    
    }

    protected virtual void Move()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    public void SetSenderRootTransform(GameObject newSenderRootGameObject)
    {
        senderRootGameObject = newSenderRootGameObject;
    }

    protected void OnTriggerEnter(Collider other)
    {
        if(other.transform.root.gameObject != senderRootGameObject)
        {
            if(DamageDealer.dealDamage(senderRootGameObject, other.gameObject, damage))
            {
                Destroy(gameObject);
                hitEffect();
            }
        }
    }

    protected void hitEffect()
    {
        if(hitEffectPrefab != null)
        {
            GameObject hitEffectObject = GameObject.Instantiate(hitEffectPrefab);

            hitEffectObject.transform.position = transform.position;
            hitEffectObject.transform.forward = transform.forward;

            Destroy(hitEffectObject, 1);
        }
    }
    
}