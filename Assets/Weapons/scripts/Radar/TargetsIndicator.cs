﻿using UnityEngine;
using UnityEngine.UI;

public class TargetsIndicator : MonoBehaviour
{
    public Transform targetTransform;
    public Camera myCamera;
    public GameObject onScreenIndicator;
    public GameObject offScreenIndicator;

    public float minWidth = 5f;
    public float minHight = 5f;

    private RectTransform rectTransform;

    // Start is called before the first frame update
    void Start()
    {
        minHight = minWidth = 50;
        rectTransform = transform.GetComponent<RectTransform>();
        onScreenIndicator.SetActive(false);
        offScreenIndicator.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(targetTransform != null)
        {
            if(IsOnScreen())
            {
                onScreenIndicator.SetActive(true);
                offScreenIndicator.SetActive(false);
                transform.position = myCamera.WorldToScreenPoint(targetTransform.position);
                transform.rotation = Quaternion.identity;
                Rect worldBounds = GUIRectWithObject();

                rectTransform.sizeDelta = new Vector2(Mathf.Max(worldBounds.width, minWidth), Mathf.Max(worldBounds.height, minHight));
            }
            else
            {
                onScreenIndicator.SetActive(false);
                offScreenIndicator.SetActive(true);

                float targetIndicatorAngle = 0f;

                Vector3 centeredViewportPos = GetViewportPosition();

                Vector2 clampedCenteredViewportPosition = new Vector2(centeredViewportPos.x, centeredViewportPos.y).normalized * 0.2f;
                        
                centeredViewportPos.x = clampedCenteredViewportPosition.x * (1 / myCamera.aspect);
                centeredViewportPos.y = clampedCenteredViewportPosition.y;

                targetIndicatorAngle = Mathf.Atan2(centeredViewportPos.y, centeredViewportPos.x) * Mathf.Rad2Deg;

                transform.rotation = Quaternion.Euler(0, 0, targetIndicatorAngle);
                Vector3 onScreenPosition = myCamera.transform.position + 250 * myCamera.transform.forward;
                onScreenPosition = myCamera.WorldToViewportPoint(onScreenPosition);

                onScreenPosition += new Vector3(Screen.width/2, Screen.height/2, 0);
                onScreenPosition += 200 * (new Vector3(Mathf.Cos(targetIndicatorAngle * Mathf.Deg2Rad), Mathf.Sin(targetIndicatorAngle * Mathf.Deg2Rad), 0f));
                transform.position = onScreenPosition;

            }
        }
        else
        {
            onScreenIndicator.SetActive(false);
            offScreenIndicator.SetActive(false);
        }
    }

    public Rect GUIRectWithObject()
    {
        Renderer targetRenderer = targetTransform.GetComponent<Renderer>();
        Vector3 cen = targetRenderer.bounds.center;
        Vector3 ext = targetRenderer.bounds.extents;
        Vector2[] extentPoints = new Vector2[8]
        {
            myCamera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z)),
            myCamera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z)),
            myCamera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z)),
            myCamera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z)),
            myCamera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z)),
            myCamera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z)),
            myCamera.WorldToScreenPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z)),
            myCamera.WorldToScreenPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z))
        };
        Vector2 min = extentPoints[0];
        Vector2 max = extentPoints[0];
        foreach(Vector2 v in extentPoints)
        {
            min = Vector2.Min(min, v);
            max = Vector2.Max(max, v);
        }
        return new Rect(min.x, min.y, max.x-min.x, max.y-min.y);
    }

    public Vector3 GetViewportPosition()
    {

        // Get the viewport position
        Vector3 pos = myCamera.transform.InverseTransformPoint(targetTransform.position);
        float sign = Mathf.Sign(pos.z);
        pos.z = Mathf.Abs(pos.z);
        pos = myCamera.transform.TransformPoint(pos);
        pos = myCamera.WorldToViewportPoint(pos);
        pos.z *= sign;

        // Center the position
        pos.x -= 0.5f;
        pos.y -= 0.5f;

        return pos;

    }

    public bool IsOnScreen()
    {
        Vector3 targetOnScreen = myCamera.WorldToViewportPoint(targetTransform.position);
        return targetOnScreen.x >= 0 && targetOnScreen.x <= 1 && targetOnScreen.y >= 0 && targetOnScreen.y <= 1 && targetOnScreen.z >= 0;
    }

    public void SetColor(Color newColor)
    {
        onScreenIndicator.GetComponentInChildren<Image>().color = newColor;
        offScreenIndicator.GetComponentInChildren<Image>().color = newColor;
    }
}
