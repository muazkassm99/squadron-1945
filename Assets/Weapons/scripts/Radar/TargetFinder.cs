using UnityEngine;
using System.Collections.Generic;
public class TargetFinder
{
    public static Transform FindNearestTarget(GameObject ship)
    {
        RoleType targetType = Role.GetTargetType(Role.GetRoleType(ship));

        GameObject[] targets = GameObject.FindGameObjectsWithTag(targetType.ToString());

        int closestIndex = -1;
        float dist = Mathf.Infinity;

        for(int i = 0;i<targets.Length;i++)
        {
            if(Vector3.Distance(targets[i].transform.position, ship.transform.position) < dist)
            {
                closestIndex = i;
                dist = Vector3.Distance(targets[i].transform.position, ship.transform.position);
            }
        }

        if(closestIndex < 0 || closestIndex > targets.Length - 1)
            return null;

        return targets[closestIndex].transform;
    }

    public static Transform GetTarget(GameObject ship, float range, float maxAngle)
    {
        RoleType targetType = Role.GetTargetType(Role.GetRoleType(ship));

        GameObject[] targets = GameObject.FindGameObjectsWithTag(targetType.ToString());

        int closestIndex = -1;
        float dist = Mathf.Infinity;

        for(int i = 0;i<targets.Length;i++)
        {
            if(Vector3.Distance(targets[i].transform.position, ship.transform.position) <= range)
            {
                float angle = Vector3.Angle(targets[i].transform.position - ship.transform.position, ship.transform.forward);
                if(angle > 180)
                {
                    angle -= 360;
                    angle *= -1;
                }

                if(angle < dist && angle <= maxAngle)
                {
                    closestIndex = i;
                    dist = angle;
                }
            }
        }

        if(closestIndex < 0 || closestIndex > targets.Length - 1)
        {
            targets = GameObject.FindGameObjectsWithTag("SpaceStation");
            for(int i = 0;i<targets.Length;i++)
            {
                Transform targetRoot = targets[i].transform.root.transform;
                Collider targetCollider = targetRoot.GetComponent<Collider>();
                if(targetCollider == null) targetCollider = targetRoot.GetComponentInChildren<Collider>();

                Vector3 targetPosition = targetRoot.position;
                if(Vector3.Distance(targetPosition, ship.transform.position) <= range)
                {
                    float angle = Vector3.Angle(targetPosition - ship.transform.position, ship.transform.forward);
                    if(angle > 180)
                    {
                        angle -= 360;
                        angle *= -1;
                    }

                    if(angle < dist && angle <= maxAngle)
                    {
                        closestIndex = i;
                        dist = angle;
                    }
                }
            }
        }

        if(closestIndex < 0 || closestIndex > targets.Length - 1)
        {
            targets = GameObject.FindGameObjectsWithTag("SpaceObject");
            for(int i = 0;i<targets.Length;i++)
            {
                Transform targetRoot = targets[i].transform.root.transform;
                Collider targetCollider = targetRoot.GetComponent<Collider>();
                if(targetCollider == null) targetCollider = targetRoot.GetComponentInChildren<Collider>();

                Vector3 targetPosition = targetRoot.position;
                if(Vector3.Distance(targetPosition, ship.transform.position) <= range)
                {
                    float angle = Vector3.Angle(targetPosition - ship.transform.position, ship.transform.forward);
                    if(angle > 180)
                    {
                        angle -= 360;
                        angle *= -1;
                    }

                    if(angle < dist && angle <= maxAngle)
                    {
                        closestIndex = i;
                        dist = angle;
                    }
                }
            }
        }

        if(closestIndex < 0 || closestIndex > targets.Length - 1)
            return null;
        
        return targets[closestIndex].transform;

    }

    public static List<GameObject> GetAllOfType(RoleType type, Transform center = null, float radius = Mathf.Infinity)
    {
        GameObject[] targets = GameObject.FindGameObjectsWithTag(type.ToString());
        List<GameObject> res = new List<GameObject>();

        for(int i = 0;i<targets.Length;i++)
        {
            if(center != null)
            {
                if(Vector3.Distance(center.position, targets[i].transform.position) <= radius)
                    res.Add(targets[i]);
            }
            else 
            {
                res.Add(targets[i]);
            }
        }

        return res;
    }
}