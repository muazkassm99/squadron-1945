public enum RoleType
{
    Ally,
    Enemy,
    SpaceObject,
    SpaceStation,
    Bullet,
    Other
}