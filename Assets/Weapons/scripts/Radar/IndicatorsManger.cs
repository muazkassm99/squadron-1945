using UnityEngine;
using System.Collections.Generic;
public class IndicatorsManger : MonoBehaviour {

    public int maxIndicatorsNumber;
    public GameObject indicatorPrefab;
    public Camera myCamera;
    public Color indicatorColorForEnemyShip;
    public Color indicatorColorForSpaceStation;
    private List<TargetsIndicator> indicators;

    void Start()
    {
        indicators = new List<TargetsIndicator>();
        for(int i = 0;i<maxIndicatorsNumber;i++)
        {
            GameObject newIndicator = Instantiate(indicatorPrefab);
            newIndicator.transform.SetParent(transform);
            
            indicators.Add(newIndicator.GetComponent<TargetsIndicator>());
            indicators[i].myCamera = myCamera;
        }
    }

   void Update()
   {
       List<GameObject> enemyShips = TargetFinder.GetAllOfType(RoleType.Enemy, myCamera.transform, 1000f);
       List<GameObject> spaceStations = TargetFinder.GetAllOfType(RoleType.SpaceStation);

       for(int i = 0;i<indicators.Count;i++)
       {
           if(i < enemyShips.Count)
           {
               indicators[i].targetTransform = FindRenderer(enemyShips[i]);
               indicators[i].SetColor(indicatorColorForEnemyShip);
           }
           else
           {
               break;
           }
       }

       for(int i = enemyShips.Count;i<indicators.Count;i++)
       {
           if(i - enemyShips.Count < spaceStations.Count)
           {
               indicators[i].targetTransform = FindRenderer(spaceStations[i - enemyShips.Count]);
               indicators[i].SetColor(indicatorColorForSpaceStation);
           }
           else
           {
               indicators[i].targetTransform = null;
           }
       }
   } 

   private Transform FindRenderer(GameObject target)
   {    
        target = target.transform.root.gameObject;
        Renderer renderer = target.GetComponent<Renderer>();
        if(renderer == null)
        {
            renderer = target.GetComponentInChildren<Renderer>();
        }

        if(renderer == null) 
        {
            return null;
        }
    
        return renderer.transform;
   }
}