using UnityEngine;
using System;

public class Role
{
    
    public static RoleType GetTargetType(RoleType type)
    {
        switch(type)
        {
            case RoleType.Ally:
                return RoleType.Enemy;
            
            case RoleType.Enemy:
                return RoleType.Ally;
            
            default:
                return RoleType.Other;
        }
    }

    public static RoleType GetRoleType(GameObject gameObject)
    {
        if(gameObject != null)
            {
            Transform rootTransform = gameObject.transform.root;

            foreach(Transform child in rootTransform)
            {   
                if(child.gameObject == gameObject) Debug.Log("lol what");
                if(child.gameObject.name == "Role")
                {
                    return (RoleType) Enum.Parse(typeof(RoleType), child.gameObject.tag);
                }
                
            }
        }

        return RoleType.Other;
    }
}