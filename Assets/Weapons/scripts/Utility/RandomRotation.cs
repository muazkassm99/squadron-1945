﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{

    [SerializeField]
    protected Transform targetTransform;

    [Header("Rotation Limits X")]

    [SerializeField]
    protected float minRotationX = 0;

    [SerializeField]
    protected float maxRotationX = 0;

    [Header("Rotation Limits Y")]

    [SerializeField]
    protected float minRotationY = 0;

    [SerializeField]
    protected float maxRotationY = 0;

    [Header("Rotation Limits Z")]

    [SerializeField]
    protected float minRotationZ = 0;

    [SerializeField]
    protected float maxRotationZ = 0;


    protected virtual void Reset()
    {
        targetTransform = transform;
    }

    public void NewRotation()
    {

        transform.localRotation = Quaternion.Euler(Random.Range(minRotationX, maxRotationX),
                                                    Random.Range(minRotationY, maxRotationY),
                                                    Random.Range(minRotationZ, maxRotationZ));
    }
}
