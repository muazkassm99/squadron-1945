using UnityEngine;

public class DestroyAfterGameObject : MonoBehaviour {
    public GameObject other;
    public float time;
    
    private float destrctionTime = -1;

    void Update() {
        if(destrctionTime == -1)
        {
            if(other == null)
                destrctionTime = Time.time;
        }
        else
        {
            if(Time.time > destrctionTime + time)
            {
                Destroy(gameObject);
            }
        }
    }

}