﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RandomScaler : MonoBehaviour
{

    [SerializeField]
    protected Transform targetTransform;

    [Header("Scale Limits X")]

    [SerializeField]
    protected float minScaleX = 0.8f;

    [SerializeField]
    protected float maxScaleX = 1.2f;

    [Header("Scale Limits Y")]

    [SerializeField]
    protected float minScaleY = 0.8f;

    [SerializeField]
    protected float maxScaleY = 1.2f;

    [Header("Scale Limits Z")]

    [SerializeField]
    protected float minScaleZ = 0.8f;

    [SerializeField]
    protected float maxScaleZ = 1.2f;


    protected void Reset()
    {
        targetTransform = transform;
    }

    public void NewScale()
    {

        transform.localScale = new Vector3(Random.Range(minScaleX, maxScaleX),
                                                    Random.Range(minScaleY, maxScaleY),
                                                    Random.Range(minScaleZ, maxScaleZ));
    }
}
