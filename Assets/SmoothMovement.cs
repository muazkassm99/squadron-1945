﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothMovement : MonoBehaviour
{

    public Transform player;

    public float smoothSpeed = 0.4f;
    public Vector3 offset = new Vector3(0f, 7f, -17f);


    private void FixedUpdate()
    {
        Vector3 newPosition = player.position + offset;
        Vector3 smoothPosition = Vector3.Lerp(transform.position, newPosition, smoothSpeed);
        /*transform.position = smoothPosition * Time.deltaTime;*/
        transform.localPosition = smoothPosition;

        transform.LookAt(player);
    }
}
