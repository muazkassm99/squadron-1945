using UnityEngine;
using System.Collections.Generic;

public class AvoidObstical : BaseMoveUtil 
{

    private float scanningRadius;
    private float lastChange;
    private float effectPeriod;
    private Vector3 steer;
    private float radius;
    private Engine movingObjectEngine;

    public AvoidObstical(float scanningRadius, float effectPeriod, float radius, Engine movingObjectEngine)
    {
        this.steer = Vector3.zero;
        this.scanningRadius = scanningRadius;
        this.effectPeriod = effectPeriod;
        this.radius = radius;
        this.movingObjectEngine = movingObjectEngine;

        lastChange = -Mathf.Infinity;
    }
    public override void PerformMove() {

        Transform movingObject = movingObjectEngine.transform;
        Collider[] hits = Physics.OverlapSphere(movingObject.position, scanningRadius);

        // Get all threatening obstacle
        Vector3 avoidanceForce = Vector3.zero;

        List<GameObject> threateningObstacleList = new List<GameObject>();
        for (int i = 0; i < hits.Length; i++) {
            GameObject dynamicObstacle = hits[i].transform.root.gameObject;

            if (dynamicObstacle != null) {
                if (hits[i].transform.root.gameObject != movingObject.root.gameObject && hits[i].gameObject.CompareTag("Obstical")) 
                {
                    Vector3 obstacleFuturePosition = hits[i].transform.position + hits[i].gameObject.GetComponent<Obstical>().velocity;
                    Vector3 futurePosition = movingObject.position + movingObjectEngine.translationSpeed * movingObject.forward;

                    float sumRadius = hits[i].gameObject.GetComponent<Obstical>().radius + radius;
                    float t = willCollide(movingObject.position - hits[i].transform.position, movingObjectEngine.translationSpeed * movingObject.forward - hits[i].gameObject.GetComponent<Obstical>().velocity, sumRadius);
                    // Debug.Log(t);
                    if (t >= 0 && t < 500) {
                        Vector3 currentAvoidanceForce = Vector3.zero;
                        currentAvoidanceForce = movingObject.position + movingObjectEngine.translationSpeed * movingObject.forward - hits[i].transform.position;

                        if (Vector3.Dot(currentAvoidanceForce, movingObjectEngine.translationSpeed * movingObject.forward) < 0) {
                            currentAvoidanceForce = movingObject.right;
                        }
                        
                        avoidanceForce += currentAvoidanceForce.normalized * (1 - t / 500);
                    }
                }
            }
        }

        if(avoidanceForce != Vector3.zero) 
        {
            steer = avoidanceForce - movingObjectEngine.translationSpeed * movingObject.forward;
            lastChange = Time.time;
        } 
        else 
        {
            if(Time.time > lastChange + effectPeriod) steer = Vector3.zero;;
        }

        //movingObjectEngine.AddSteer(steer*100);
        movingObjectEngine.AcceptRotation(steer * 100);
        // Debug.Log(steer);
    }

    float willCollide(Vector3 positionDiff, Vector3 velocityDiff, float sumRadius) {
        float a = velocityDiff.sqrMagnitude;
        float b = Vector3.Dot(positionDiff, velocityDiff) * 2;
        float c = positionDiff.sqrMagnitude - sumRadius;

        float delta = b * b - 4 * a * c;
        if(delta < 0) 
            return -1;

        delta = Mathf.Sqrt(delta);
        float t1 = (-b + delta) / (2 * a);
        float t2 = (-b - delta) / (2 * a);
        
        return (Mathf.Min(t1, t2) >= 0) ? Mathf.Min(t1, t2) : Mathf.Max(t1, t2);
    }
}