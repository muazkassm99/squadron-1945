﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointsSystem : MonoBehaviour
{

    [SerializeField]
    private List<Transform> path;
    [SerializeField]
    private float arriveDistance;

    public Vector3 GetStartingPoint() 
    {
        return path[(int) Random.Range(0, path.Count - 0.1f)].position;
    }
    public Vector3 GetNextPoint(Vector3 currentPoint)
    {
        for(int i = 0;i<path.Count;i++)
        {
            if(path[i].position == currentPoint)
            {
                return path[(i+1)%path.Count].position;
            }
        }

        return GetStartingPoint();
    }

    public bool arrived(Vector3 position, Vector3 currentPoint)
    {
        return (Vector3.Distance(position, currentPoint) <= arriveDistance);
    }
    
}
