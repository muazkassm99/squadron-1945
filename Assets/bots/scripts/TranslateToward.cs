using UnityEngine;

public class TranslateToward : BaseMoveUtil 
{
    private Vector3 target;
    private Engine movingObjectEngine;

    public TranslateToward(Vector3 target, Engine movingObjectEngine)
    {
        this.target = target;
        this.movingObjectEngine = movingObjectEngine;
    }
    public override void PerformMove() 
    {
        movingObjectEngine.AcceptTranslation(target);
    }
}