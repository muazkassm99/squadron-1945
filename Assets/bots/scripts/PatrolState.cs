using UnityEngine;

public class PatrolState : OpenWorldState
{
    private OpenWorldBotBehavior behavior;
    private Transform bot;
    private Engine botEngine;
    private BaseMoveUtil patrolMove;
    private float homeRadius;
    private Vector3 homeCinter;
    private WayPointsSystem patrolPath;
    public PatrolState(Transform bot, Engine botEngine, OpenWorldBotBehavior behavior)
    {
        this.bot = bot;
        this.botEngine = botEngine;
        this.behavior = behavior;
    }
    public void OnStateEnter()
    {
        this.homeCinter = behavior.GetHomeCinter();
        this.homeRadius = behavior.GetHomeRadius();
        this.patrolPath = behavior.GetPatrolPath();

        // patrolMove = new Wander(behavior.GetWanderRadius(), behavior.GetWanderDistance(), behavior.GetRandomPointFrequency());
        patrolMove = new FollowPath(patrolPath, botEngine);
        //Debug.Log(patrolPath);
    }

    public void Update()
    {
        Move();

        if(Vector3.Distance(bot.position, Radar.getTarget(bot).position) < behavior.GetDist())
        {
            behavior.setState(new OffensiveState(bot, botEngine, behavior.GetAttackTimeRange(), behavior));
        }  
        else if(Vector3.Distance(bot.position, homeCinter) > homeRadius)
        {
            behavior.setState(new OutOfRangeState(bot, botEngine, behavior));
        }
    }

    private void Move()
    {
        patrolMove.PerformMove();
        botEngine.AcceptTranslation();
    }
}