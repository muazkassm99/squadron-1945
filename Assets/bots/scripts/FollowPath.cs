using UnityEngine;

public class FollowPath : BaseMoveUtil
{
    private WayPointsSystem path;
    private Vector3 currentPoint;
    private Engine movingObjectEngine;
    public FollowPath(WayPointsSystem path, Engine movingObjectEngine)
    {
        this.path = path;
        this.movingObjectEngine = movingObjectEngine;
        currentPoint = path.GetStartingPoint();
    }

    public override void PerformMove()
    {
        Transform movingObject = movingObjectEngine.transform;
        // Debug.Log(currentPoint);
        if(path.arrived(movingObject.position, currentPoint))
        {
            currentPoint = path.GetNextPoint(currentPoint);
        }

        TurnToward turnMovement = new TurnToward(currentPoint, movingObjectEngine);

        turnMovement.PerformMove();
    }
}