using UnityEngine;

public class DefensiveState : OpenWorldState
{
    private OpenWorldBotBehavior behavior;
    private BaseMoveUtil followTarget;
    private float startTime;
    private float evadePeriod;
    private Vector2 evadeTimeRange;
    private Transform bot;
    private Engine botEngine;
    private Transform target;
    private float evadeAngle;
    private Vector3 evadeDirection;
    private float returnToHomeFactor;

    public DefensiveState(Transform bot, Engine botEngine, Vector2 evadeTimeRange, float evadeAngle, float returnToHomeFactor, OpenWorldBotBehavior behavior)
    {
        this.bot = bot;
        this.botEngine = botEngine;
        this.evadeTimeRange = evadeTimeRange;
        this.evadeAngle = evadeAngle;
        this.returnToHomeFactor = returnToHomeFactor;
        this.behavior = behavior;
        this.target = GameObject.Find("Player").transform;
    }

    public void OnStateEnter()
    {
        startTime = Time.time;
        evadePeriod = Random.Range(evadeTimeRange.x, evadeTimeRange.y);
        UpdateEvadeDirection();
    }

    public void Update()
    {
        Move();

        if(Time.time > startTime + evadePeriod)
        {
            behavior.setState(new OffensiveState(bot, botEngine, behavior.GetAttackTimeRange(), behavior));
        }
    }

    public void Move()
    {
        Vector3 targetPosition = bot.position + Weave(evadeDirection);
        (new TurnToward(targetPosition, botEngine)).PerformMove();
        botEngine.AcceptTurbo();
        botEngine.AcceptTranslation();
        //botEngine.AddBoost();
        
        //botEngine.Move();
    }

    private void UpdateEvadeDirection()
    {
        Vector3 toTargetDirection = (target.position - bot.position).normalized;

        evadeDirection = (Quaternion.Euler(new Vector3(0f, Random.Range(-evadeAngle, evadeAngle), 0f)) * -toTargetDirection).normalized;

        Vector3 toCenterVector = behavior.GetHomeCinter() - bot.position;

        float returnToCenterStrength = Mathf.Clamp(toCenterVector.magnitude * returnToHomeFactor, 0f, 1f);
        evadeDirection = (returnToCenterStrength * toCenterVector.normalized + (1 - returnToCenterStrength) * evadeDirection.normalized).normalized;
    }

    private Vector3 Weave(Vector3 pathDirection)
    {
        float offsetX = (Mathf.PerlinNoise(Time.time * 0.6f, 0f) - 0.5f) * 2f;

        float offsetY = (Mathf.PerlinNoise(0f, Time.time * 0.6f) - 0.5f) * 2f;

        Vector3 offsetVec = new Vector3(offsetX, offsetY, 1).normalized * 1.5f;
        Vector3 offset = new Vector3(offsetVec.x, offsetVec.y, 1).normalized;

        pathDirection = Quaternion.FromToRotation(Vector3.forward, pathDirection) * offset;

        return pathDirection;

    }
}