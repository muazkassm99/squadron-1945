public interface OpenWorldState
{
    void OnStateEnter();
    void Update();
}