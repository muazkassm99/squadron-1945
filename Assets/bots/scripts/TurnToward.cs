using UnityEngine;

public class TurnToward : BaseMoveUtil 
{
    private Vector3 target;
    private Engine movingObjectEngine;

    public TurnToward(Vector3 target, Engine movingObjectEngine)
    {
        this.target = target;
        this.movingObjectEngine = movingObjectEngine;
    }
    public override void PerformMove() 
    {  
        movingObjectEngine.AcceptRotation(Vector3.Cross(movingObjectEngine.transform.forward, target - movingObjectEngine.transform.position));
    }
}