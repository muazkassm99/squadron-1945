using UnityEngine;
using System.Collections.Generic;

public class Radar
{
    public static Transform getTarget(Transform bot)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        int closestIndex = 0;
        float dist = Mathf.Infinity;

        for(int i = 0;i<players.Length;i++)
        {
            if(Vector3.Distance(players[i].transform.position, bot.position) < dist)
            {
                closestIndex = i;
                dist = Vector3.Distance(players[i].transform.position, bot.position);
            }
        }

        return players[closestIndex].transform;
    }
}